import 'features/idiom/data/data_sources/idiom_local_data_source.dart';
import 'features/idiom/data/repositories/idiom_repository_impl.dart';
import 'features/idiom/domain/repositories/idiom_repository.dart';
import 'features/idiom/domain/usecases/create_idiom_use_case.dart';
import 'features/idiom/domain/usecases/read_all_idioms_use_case.dart';
import 'features/idiom/domain/usecases/read_one_idiom_use_case.dart';
import 'features/idiom/presentation/bloc/read_all_idioms_bloc.dart';

import 'features/idiom/presentation/bloc/crud_idiom_bloc.dart';

import 'features/word/domain/usecases/update_word_use_case.dart';

import 'features/word/domain/usecases/read_one_word_use_case.dart';
import 'features/word/presentation/bloc/read_all_words_bloc.dart';

import 'core/database/database.dart';
import 'core/services/api_service.dart';
import 'features/word/data/data_sources/word_local_data_source.dart';
import 'features/word/data/data_sources/word_remote_data_source.dart';
import 'features/word/data/repositories/word_repository_impl.dart';
import 'features/word/domain/repositories/word_repository.dart';
import 'features/word/domain/usecases/create_word_use_case.dart';
import 'features/word/domain/usecases/check_stored_use_case.dart';
import 'features/word/domain/usecases/delete_word_use_case.dart';
import 'features/word/domain/usecases/read_all_words_use_case.dart';
import 'features/word/presentation/bloc/crud_word_bloc.dart';
import 'features/word/presentation/bloc/search_words_online_bloc.dart';
import 'package:get_it/get_it.dart';

import 'features/word/domain/usecases/get_word_meaning_from_dictionary_api_use_case.dart';

final sl = GetIt.instance;

void init() {
  //word
  //bloc
  sl.registerFactory<SearchWordsOnlineBloc>(() => SearchWordsOnlineBloc(sl(), sl()));
  sl.registerFactory<CrudWordBloc>(() => CrudWordBloc(sl(), sl(), sl(), sl()));
  sl.registerFactory<ReadAllWordsBloc>(() => ReadAllWordsBloc(sl()));

  //usecases
  sl.registerLazySingleton<GetWordMeaningFromDictionaryApiUseCase>(() => GetWordMeaningFromDictionaryApiUseCase(sl()));
  sl.registerLazySingleton<CheckStoredUseCase>(() => CheckStoredUseCase(sl()));
  sl.registerLazySingleton<CreateWordUseCase>(() => CreateWordUseCase(sl()));
  sl.registerLazySingleton<ReadAllWordsUseCase>(() => ReadAllWordsUseCase(sl()));
  sl.registerLazySingleton<DeleteWordUseCase>(() => DeleteWordUseCase(sl()));
  sl.registerLazySingleton<ReadOneWordUseCase>(() => ReadOneWordUseCase(sl()));
  sl.registerLazySingleton<UpdateWordUseCase>(() => UpdateWordUseCase(sl()));

  //repositories
  sl.registerLazySingleton<WordRepository>(() => WordRepositoryImpl(sl(), sl()));

  //datasources
  sl.registerLazySingleton<WordRemoteDataSource>(() => WordRemoteDataSourceImpl(sl()));
  sl.registerLazySingleton<WordLocalDataSource>(() => WordLocalDataSourceImpl(sl()));

  // ************************************************************* //

  //idiom
  //bloc
  sl.registerFactory<CrudIdiomBloc>(() => CrudIdiomBloc(sl(), sl()));
  sl.registerFactory<ReadAllIdiomsBloc>(() => ReadAllIdiomsBloc(sl()));

  //usecases
  sl.registerLazySingleton<CreateIdiomUseCase>(() => CreateIdiomUseCase(sl()));
  sl.registerLazySingleton<ReadAllIdiomsUseCase>(() => ReadAllIdiomsUseCase(sl()));
  sl.registerLazySingleton<ReadOneIdiomUseCase>(() => ReadOneIdiomUseCase(sl()));

  //repositories
  sl.registerLazySingleton<IdiomRepository>(() => IdiomRepositoryImpl(sl()));

  //datasources
  sl.registerLazySingleton<IdiomLocalDataSource>(() => IdiomLocalDataSourceImpl(sl()));

  // ************************************************************* //
  
  //external
  sl.registerLazySingleton<ApiService>(() => ApiService());

  //core
  sl.registerLazySingleton<DB>(() => DB.instance);
}
