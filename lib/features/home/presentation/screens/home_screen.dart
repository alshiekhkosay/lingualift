import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';

import '../../../../core/routes/app_pages.dart';
import '../../../idiom/presentation/bloc/read_all_idioms_bloc.dart';
import '../../../word/presentation/bloc/read_all_words_bloc.dart';
import '../../../word/presentation/bloc/search_words_online_bloc.dart';
import 'widgets/floating_actions_buttons.dart';
import 'widgets/home_screen_top_bar.dart';
import 'widgets/recently_added_idioms.dart';
import 'widgets/recently_added_words.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> with TickerProviderStateMixin {
  @override
  void initState() {
    context.read<ReadAllWordsBloc>().add(ReadAllWordEvent());
    context.read<ReadAllIdiomsBloc>().add(ReadAllIdiomEvent());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<SearchWordsOnlineBloc, SearchWordsOnlineState>(
      listener: (context, state) {
        if (state is Success) {
          Get.toNamed(Routes.WORD, arguments: {"isSearching": true, "word": state.word});
        }
      },
      child: GestureDetector(
        onTap: () {
          FocusScope.of(context).unfocus();
        },
        child: const Scaffold(
          floatingActionButton: FLoatingActionsButtons(),
          body: SingleChildScrollView(
              child: Column(
            children: [
              HomeScreenTopBar(),
              RecentlyAddedWords(),
              RecentlyAddedIdioms(),
            ],
          )),
        ),
      ),
    );
  }
}
