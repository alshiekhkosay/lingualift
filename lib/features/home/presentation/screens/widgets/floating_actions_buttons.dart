import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:lottie/lottie.dart';

import '../../../../../core/routes/app_pages.dart';

class FLoatingActionsButtons extends StatefulWidget {
  const FLoatingActionsButtons({super.key});

  @override
  State<FLoatingActionsButtons> createState() => _FLoatingActionsButtonsState();
}

class _FLoatingActionsButtonsState extends State<FLoatingActionsButtons> with SingleTickerProviderStateMixin {
  bool adding = false;
  late AnimationController animationController;
  @override
  void initState() {
    animationController = AnimationController(vsync: this, duration: const Duration(milliseconds: 500));

    super.initState();
  }

  @override
  void dispose() {
    animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        AnimatedScale(
          scale: adding ? 1 : 0,
          duration: const Duration(milliseconds: 400),
          curve: Curves.easeInOutQuad,
          child: FloatingActionButton(
            heroTag: 0,
            onPressed: () {
              setState(() {
                adding = false;
              });
              animationController.animateTo(0);
              Get.toNamed(Routes.ADD_UPDATE_WORD, arguments: {'isEditing': false});
            },
            backgroundColor: Colors.blue,
            child: Text(
              "Word",
              style: Theme.of(context).textTheme.labelMedium?.copyWith(color: Colors.white),
            ),
          ),
        ),
        const SizedBox(height: 5),
        AnimatedScale(
          scale: adding ? 1 : 0,
          curve: Curves.easeInOutQuad,
          duration: const Duration(milliseconds: 400),
          child: FloatingActionButton(
            heroTag: 1,
            onPressed: () {
              setState(() {
                adding = false;
              });
              animationController.animateTo(0);
              Get.toNamed(Routes.ADD_UPDATE_IDIOM, arguments: {'isEditing': false});
            },
            backgroundColor: Colors.blue,
            child: Text(
              "Idom",
              style: Theme.of(context).textTheme.labelMedium?.copyWith(color: Colors.white),
            ),
          ),
        ),
        const SizedBox(height: 5),
        FloatingActionButton(
            heroTag: 2,
            onPressed: () {
              // Get.toNamed(Routes.ADD_UPDATE, arguments: {'isEditing': false});
              if (!animationController.isAnimating) {
                setState(() {
                  adding = !adding;
                });
                if (animationController.value == 0) {
                  animationController.animateTo(0.5);
                } else if (animationController.value >= 0.5) {
                  animationController.animateTo(0);
                }
              }
            },
            backgroundColor: Colors.blue,
            child: SizedBox(
              height: 28,
              width: 28,
              child: Lottie.asset(
                "assets/icons/add_animated.json",
                controller: animationController,
              ),
            )),
      ],
    );
  }
}
