import 'package:flutter/material.dart';

import 'search_field.dart';

class HomeScreenTopBar extends StatelessWidget {
  const HomeScreenTopBar({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 300,
      alignment: Alignment.bottomCenter,
      decoration: BoxDecoration(
          gradient: LinearGradient(
        colors: [
          Colors.blue[200]!,
          Colors.blue[900]!,
        ],
        stops: const [0.1, 1],
        begin: Alignment.bottomCenter,
        end: Alignment.topCenter,
      )),
      padding: const EdgeInsets.all(25),
      child: Column(
        children: [
          Expanded(
              child: Center(
            child: Text(
              "LINGUA LIFT",
              style: Theme.of(context).textTheme.displayLarge?.copyWith(color: Colors.white),
            ),
          )),
          const SearchField(),
        ],
      ),
    );
  }
}
