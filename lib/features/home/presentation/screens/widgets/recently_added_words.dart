import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';

import '../../../../../core/routes/app_pages.dart';
import '../../../../word/presentation/bloc/read_all_words_bloc.dart';
import 'recent_word_card.dart';

class RecentlyAddedWords extends StatelessWidget {
  const RecentlyAddedWords({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ReadAllWordsBloc, ReadAllWordsState>(
      builder: (context, state) {
        if (state is ReadAllLoading) {
          return const Center(child: CircularProgressIndicator());
        }
        if (state is ReadAllSuccess) {
          return Padding(
            padding: const EdgeInsets.symmetric(horizontal: 24),
            child: SizedBox(
              width: double.infinity,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 12),
                    child: Text(
                      "Recent words",
                      style: Theme.of(context).textTheme.titleMedium,
                    ),
                  ),
                  if (state.words.isNotEmpty)
                    SizedBox(
                      height: 150,
                      child: SingleChildScrollView(
                        scrollDirection: Axis.horizontal,
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: state.words
                              .map((word) => RecentCard(
                                    title: word.word,
                                    callBack: () {
                                      Get.toNamed(Routes.WORD, arguments: {"word": word});
                                    },
                                  ))
                              .toList(),
                        ),
                      ),
                    )
                  else
                    const Center(child: Text("Nothing to show"))
                ],
              ),
            ),
          );
        }
        return Container();
      },
    );
  }
}
