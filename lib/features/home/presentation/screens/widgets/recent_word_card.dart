import 'package:flutter/material.dart';

class RecentCard extends StatelessWidget {
  const RecentCard({super.key, required this.title, required this.callBack});
  final String title;
  final Function() callBack;
  @override
  Widget build(BuildContext context) {
    return Container(
        height: 135,
        width: 150,
        padding: const EdgeInsets.all(12),
        margin: const EdgeInsets.only(right: 12),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(16),
          boxShadow: [
            BoxShadow(color: Colors.black.withOpacity(0.1), offset: const Offset(2, 3), blurRadius: 8),
          ],
        ),
        alignment: Alignment.center,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const Spacer(),
            Text(
              title,
              style: Theme.of(context).textTheme.titleSmall,
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
            ),
            const Spacer(),
            TextButton(
              onPressed: callBack,
              style: ButtonStyle(
                backgroundColor: const MaterialStatePropertyAll(Colors.blue),
                shape: MaterialStatePropertyAll(ContinuousRectangleBorder(borderRadius: BorderRadius.circular(14), side: BorderSide.none)),
              ),
              child: Text(
                "See Details",
                style: Theme.of(context).textTheme.labelMedium?.copyWith(color: Colors.white),
              ),
            )
          ],
        ));
  }
}
