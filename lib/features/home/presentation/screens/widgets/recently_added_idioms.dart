import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';
import 'recent_word_card.dart';

import '../../../../../core/routes/app_pages.dart';
import '../../../../idiom/presentation/bloc/read_all_idioms_bloc.dart';

class RecentlyAddedIdioms extends StatelessWidget {
  const RecentlyAddedIdioms({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ReadAllIdiomsBloc, ReadAllIdiomsState>(
      builder: (context, state) {
        if (state is ReadAllLoading) {
          return const Center(child: CircularProgressIndicator());
        }
        if (state is ReadAllSuccess) {
          return Padding(
            padding: const EdgeInsets.symmetric(horizontal: 24),
            child: SizedBox(
              width: double.infinity,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 12),
                    child: Text(
                      "Recent idioms",
                      style: Theme.of(context).textTheme.titleMedium,
                    ),
                  ),
                  if (state.idioms.isNotEmpty)
                    SizedBox(
                      height: 150,
                      child: SingleChildScrollView(
                        scrollDirection: Axis.horizontal,
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: state.idioms
                              .map((idiom) => RecentCard(
                                    title: idiom.idiom,
                                    callBack: () {
                                      Get.toNamed(Routes.IDIOM, arguments: idiom.id);
                                    },
                                  ))
                              .toList(),
                        ),
                      ),
                    )
                  else
                    const Center(child: Text("Nothing to show"))
                ],
              ),
            ),
          );
        }
        return Container();
      },
    );
  }
}
