import 'package:dartz/dartz.dart';
import '../../../../core/error/exeptions.dart';

import '../../../../core/constants/messages.dart';
import '../../../../core/error/failure.dart';
import '../../../../core/mappers/idiom_mapper.dart';

import '../../domain/entities/idiom.dart';

import '../../domain/repositories/idiom_repository.dart';
import '../data_sources/idiom_local_data_source.dart';

class IdiomRepositoryImpl implements IdiomRepository {
  final IdiomLocalDataSource idiomLocalDataSource;
  IdiomRepositoryImpl(this.idiomLocalDataSource);

  @override
  Future<Either<Failure, void>> createIdiom(Idiom idiom) async {
    try {
      final idiomDTO = IdiomDTO.fromDomain(idiom);
      await idiomLocalDataSource.createIdiom(idiomDTO.toData());
      return const Right(null);
    } on CacheException {
      return const Left(CacheFailure(IDOM_CREATE_FAILURE_MESSAGE));
    }
  }

  @override
  Future<Either<Failure, List<Idiom>>> readAllIdioms() async {
    try {
      final idiomModels = await idiomLocalDataSource.readAllIdioms();
      return Right(idiomModels);
    } on CacheException {
      return const Left(CacheFailure(UNKOWN_ERROR));
    }
  }

  @override
  Future<Either<Failure, Idiom>> readOneIdiom(int id) async {
    try {
      return Right(await idiomLocalDataSource.readOneIdiom(id));
    } on CacheException {
      return const Left(CacheFailure(IDIOM_NOT_FOUND_MESSAGE));
    }
  }
}
