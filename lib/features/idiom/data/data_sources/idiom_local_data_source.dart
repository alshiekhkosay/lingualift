import 'package:flutter/foundation.dart';
import 'package:sqflite/sqflite.dart';

import '../../../../core/database/database.dart';
import '../../../../core/error/exeptions.dart';
import '../models/idiom_model.dart';

abstract class IdiomLocalDataSource {
  Future<void> createIdiom(IdiomModel idiomModel);

  Future<List<IdiomModel>> readAllIdioms();

  Future<IdiomModel> readOneIdiom(int id);
}

class IdiomLocalDataSourceImpl implements IdiomLocalDataSource {
  final DB db;

  IdiomLocalDataSourceImpl(this.db);
  @override
  Future<List<IdiomModel>> readAllIdioms() async {
    try {
      final dataRows = await db.fetch(DB.idioms);
      final dataMapRows = dataRows.map<Map<String, dynamic>>((e) {
        return {
          ...e,
          'examples': (e['examples'] as String).split('|'),
        };
      }).toList();
      return dataMapRows.map((e) => IdiomModel.fromMap(e)).toList();
    } on DatabaseException {
      throw CacheException();
    }
  }

  @override
  Future<void> createIdiom(IdiomModel idiomModel) async {
    try {
      final data = idiomModel.toMap();
      data['examples'] = (data['examples'] as List).join("|");
      await db.insert(DB.idioms, data);
    } on DatabaseException {
      throw CacheException();
    }
  }

  @override
  Future<IdiomModel> readOneIdiom(int id) async {
    try {
      final idiomRows = await db.fetch(DB.idioms, where: "id = $id");
      debugPrint(idiomRows.toString());
      if (idiomRows.isEmpty) throw CacheException();
      final dataMap = {
        ...idiomRows.first,
        'examples': (idiomRows.first['examples'] as String).split('|'),
      };
      return IdiomModel.fromMap(dataMap);
    } on DatabaseException {
      throw CacheException();
    }
  }
}
