// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'dart:convert';

import 'package:lingualift/core/enum/idiom_usage.dart';

import '../../domain/entities/idiom.dart';

class IdiomModel extends Idiom {
  const IdiomModel({
    super.id,
    required super.idiom,
    required super.meaning,
    required super.usage,
    required super.examples,
  });

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'idiom': idiom,
      'meaning': meaning,
      'usage': usage.text,
      'examples': examples,
    };
  }

  factory IdiomModel.fromMap(Map<String, dynamic> map) {
    return IdiomModel(
        id: map['id'] as int?,
        idiom: map['idiom'] as String,
        meaning: map['meaning'] as String,
        usage: IdiomUsage.mapIdiom(map["usage"]),
        examples: List<String>.from(
          (map['examples'] as List<String>),
        ));
  }

  String toJson() => json.encode(toMap());

  factory IdiomModel.fromJson(String source) => IdiomModel.fromMap(json.decode(source) as Map<String, dynamic>);
}
