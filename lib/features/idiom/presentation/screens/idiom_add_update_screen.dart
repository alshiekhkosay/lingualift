import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';

import '../../../../core/components/app_button.dart';
import '../../../../core/components/app_text_filed.dart';
import '../../../../core/enum/idiom_usage.dart';
import '../../../../core/routes/app_pages.dart';
import '../../../../core/utils/alerts.dart';
import '../../../word/presentation/widgets/word_add_update_screen/add_meaning_button.dart';
import '../../domain/entities/idiom.dart';
import '../bloc/crud_idiom_bloc.dart';

class IdiomAddUpdateScreen extends StatefulWidget {
  const IdiomAddUpdateScreen({super.key});

  @override
  State<IdiomAddUpdateScreen> createState() => _IdiomAddUpdateScreenState();
}

class _IdiomAddUpdateScreenState extends State<IdiomAddUpdateScreen> {
  late TextEditingController idiomTextController;
  late TextEditingController meaningTextController;

  late bool isEditing;

  late Idiom idiom;
  late List<String> examples;
  late IdiomUsage idiomUsage;
  @override
  void initState() {
    init();

    super.initState();
  }

  void init() {
    idiomTextController = TextEditingController();
    meaningTextController = TextEditingController();

    isEditing = Get.arguments["isEditing"];
    if (isEditing) {
      idiom = Get.arguments["idiom"];
      idiomTextController.text = idiom.idiom;
      meaningTextController.text = idiom.meaning;
      examples = idiom.examples;
      idiomUsage = idiom.usage;
    } else {
      idiom = Idiom.empty;
      examples = [];
      idiomUsage = IdiomUsage.none;
    }
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).unfocus();
      },
      child: Scaffold(
          body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 24),
        child: CustomScrollView(slivers: [
          SliverToBoxAdapter(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const SizedBox(height: 50),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Expanded(
                      child: AppTextField(
                        controller: idiomTextController,
                        textStyle: Theme.of(context).textTheme.bodyLarge!,
                        hintText: "write idiom",
                        hintStyle: Theme.of(context).textTheme.bodyLarge?.copyWith(color: Colors.grey),
                        validator: (value) {
                          if (value != null && value.trim().isEmpty) {
                            return "this field can not be empty";
                          }
                          return null;
                        },
                      ),
                    ),
                    const SizedBox(width: 20),
                    GestureDetector(
                        onTap: () {
                          if (idiomTextController.text.isNotEmpty) {
                            final url = "https://dictionary.cambridge.org/dictionary/english/${idiomTextController.text}";
                            Get.toNamed(Routes.WEB_SCREEN, arguments: url);
                          }
                        },
                        child: const Icon(CupertinoIcons.search))
                  ],
                ),
                const SizedBox(height: 5),
                AppTextField(
                  controller: meaningTextController,
                  hintText: "meaning",
                  textStyle: Theme.of(context).textTheme.bodyMedium!,
                  hintStyle: Theme.of(context).textTheme.bodyMedium?.copyWith(color: Colors.grey),
                ),
                const SizedBox(height: 15),
                SizedBox(
                  width: MediaQuery.of(context).size.width,
                  height: 50,
                  child: DropdownButton(
                    items: IdiomUsage.getIdioms()
                        .map((e) => DropdownMenuItem(
                              value: e,
                              child: Text(
                                e.text,
                                style: Theme.of(context).textTheme.bodyMedium,
                              ),
                            ))
                        .toList(),
                    value: null,
                    borderRadius: BorderRadius.circular(15),
                    underline: const SizedBox(),
                    isExpanded: true,
                    hint: Text(
                      idiomUsage == IdiomUsage.none ? "choose idiom usage" : idiomUsage.text,
                      style: Theme.of(context).textTheme.bodyMedium,
                    ),
                    onChanged: (value) {
                      setState(() {
                        idiomUsage = value!;
                      });
                    },
                  ),
                ),
                const SizedBox(height: 15),
                if (examples.isNotEmpty) Text("Examples", style: Theme.of(context).textTheme.titleSmall?.copyWith(color: Colors.orange)),
                const Divider(
                  color: Colors.orange,
                  height: 0,
                  thickness: 2,
                ),
                const SizedBox(height: 5),
                ...getExamples(),
                const SizedBox(height: 20),
                Center(
                    child: AddButton(
                        callBack: () {
                          examples.add("");
                          setState(() {});
                        },
                        child: Text(
                          "Add Example",
                          style: Theme.of(context).textTheme.labelMedium,
                        )))
              ],
            ),
          ),
          SliverFillRemaining(
            hasScrollBody: false,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                AppButton(
                  callBack: () {
                    if (idiomUsage == IdiomUsage.none) {
                      return Alerts.showErrorSnackBar(context, "You need to choose idiom usage");
                    }
                    idiom = Idiom(
                        id: idiom.id, idiom: idiomTextController.text, meaning: meaningTextController.text, usage: idiomUsage, examples: examples);
                    context.read<CrudIdiomBloc>().add(CreateIdiomEvent(idiom));
                  },
                  child: Text(
                    isEditing ? "Update" : "Add Idiom",
                    style: const TextStyle(color: Colors.white),
                  ),
                ),
                const SizedBox(height: 20)
              ],
            ),
          )
        ]),
      )),
    );
  }

  getExamples() {
    return examples.asMap().entries.map((entry) {
      return EditialeIdiomExample(
        key: ValueKey(entry.value),
        entry: entry,
        isNew: entry.key >= idiom.examples.length,
        addOrUpdate: (edited, {example}) {
          if (edited) {
            examples[entry.key] = example!;
            setState(() {});
            return;
          }
          examples.removeAt(entry.key);
          setState(() {});
        },
      );
    }).toList();
  }
}

class EditialeIdiomExample extends StatefulWidget {
  const EditialeIdiomExample({
    super.key,
    required this.entry,
    required this.addOrUpdate,
    required this.isNew,
  });
  final MapEntry<int, String> entry;
  final Function(bool, {String? example}) addOrUpdate;
  final bool isNew;
  @override
  State<EditialeIdiomExample> createState() => _EditialeIdiomExampleState();
}

class _EditialeIdiomExampleState extends State<EditialeIdiomExample> {
  late bool _isEditing;
  late TapGestureRecognizer _tapGestureRecognizer;
  late TextEditingController _exampleController;
  @override
  void initState() {
    _isEditing = widget.isNew;
    _tapGestureRecognizer = TapGestureRecognizer()..onTap = _handleOnTap;
    _exampleController = TextEditingController()..text = widget.entry.value;
    super.initState();
  }

  void _handleOnTap() {
    setState(() {
      _isEditing = true;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          Expanded(
            child: !_isEditing
                ? RichText(
                    text: TextSpan(
                      text: "${widget.entry.key + 1}. ",
                      style: Theme.of(context).textTheme.labelLarge?.copyWith(color: Colors.blue),
                      children: [
                        if (widget.entry.value.isNotEmpty)
                          TextSpan(text: widget.entry.value, recognizer: _tapGestureRecognizer, style: Theme.of(context).textTheme.bodySmall)
                        else
                          TextSpan(
                              text: "tap to edit",
                              style: Theme.of(context).textTheme.bodySmall?.copyWith(color: Colors.grey),
                              recognizer: _tapGestureRecognizer)
                      ],
                    ),
                  )
                : AppTextField(
                    controller: _exampleController,
                    textStyle: const TextStyle(fontSize: 13),
                  ),
          ),
          const SizedBox(width: 20),
          GestureDetector(
              onTap: () {
                widget.addOrUpdate(_isEditing, example: _exampleController.text);
                _isEditing = !_isEditing;
              },
              child: Icon(
                !_isEditing ? Icons.delete_outline_rounded : Icons.check_rounded,
              ))
        ],
      ),
    );
  }
}
