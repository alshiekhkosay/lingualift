import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';

import '../bloc/crud_idiom_bloc.dart';

class IdiomScreen extends StatefulWidget {
  const IdiomScreen({super.key});

  @override
  State<IdiomScreen> createState() => _IdiomScreenState();
}

class _IdiomScreenState extends State<IdiomScreen> {
  @override
  void initState() {
    init();
    super.initState();
  }

  void init() {
    final id = Get.arguments as int;
    context.read<CrudIdiomBloc>().add(ReadOneIdiomEvent(id));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocBuilder<CrudIdiomBloc, CrudIdiomState>(
        builder: (context, state) {
          if (state is ReadOneIdiomSuccess) {
            debugPrint(state.idiom.toString());
            return Center(child: Text(state.idiom.idiom));
          }
          if (state is ReadOneIdiomFailed) return Center(child: Text(state.message));
          return Container();
        },
      ),
    );
  }
}
