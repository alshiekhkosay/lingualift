import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import '../../../../core/usecase/usecase.dart';
import '../../domain/usecases/read_all_idioms_use_case.dart';

import '../../domain/entities/idiom.dart';

part 'read_all_idioms_event.dart';
part 'read_all_idioms_state.dart';

class ReadAllIdiomsBloc extends Bloc<ReadAllIdiomsEvent, ReadAllIdiomsState> {
  final ReadAllIdiomsUseCase readAllIdioms;
  ReadAllIdiomsBloc(this.readAllIdioms) : super(ReadAllIdiomsInitial()) {
    _onEventListener();
  }

  void _onEventListener() {
    on<ReadAllIdiomsEvent>((event, emit) async {
      if (event is ReadAllIdiomEvent) await _handleReadAllIdiomsEvent(event, emit);
    });
  }

  Future<void> _handleReadAllIdiomsEvent(ReadAllIdiomsEvent event, Emitter<ReadAllIdiomsState> emit) async {
    emit(ReadAllLoading());
    final idiomsOrFailure = await readAllIdioms(NoParams());
    idiomsOrFailure.fold((failure) {
      emit(ReadAllFailed(failure.message));
    }, (idioms) {
      emit(ReadAllSuccess(idioms));
    });
  }
}
