part of 'crud_idiom_bloc.dart';

sealed class CrudIdiomState extends Equatable {
  final List<Object> properties;
  const CrudIdiomState({this.properties = const []});

  @override
  List<Object> get props => properties;
}

final class CrudIdiomInitial extends CrudIdiomState {}

final class ReadingOneIdiom extends CrudIdiomState {}

final class ReadOneIdiomSuccess extends CrudIdiomState {
  final Idiom idiom;

  ReadOneIdiomSuccess(this.idiom) : super(properties: [idiom]);
}

final class ReadOneIdiomFailed extends CrudIdiomState {
  final String message;

  ReadOneIdiomFailed(this.message) : super(properties: [message]);
}
