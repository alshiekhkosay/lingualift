part of 'read_all_idioms_bloc.dart';

sealed class ReadAllIdiomsState extends Equatable {
  final List<Object> properties;
  const ReadAllIdiomsState({this.properties = const []});

  @override
  List<Object> get props => properties;
}

final class ReadAllIdiomsInitial extends ReadAllIdiomsState {}

final class ReadAllLoading extends ReadAllIdiomsState {}

final class ReadAllSuccess extends ReadAllIdiomsState {
  final List<Idiom> idioms;

  ReadAllSuccess(this.idioms):super(properties: [...idioms]);
}

final class ReadAllFailed extends ReadAllIdiomsState {
  final String message;

  ReadAllFailed(this.message):super(properties: [message]);
}