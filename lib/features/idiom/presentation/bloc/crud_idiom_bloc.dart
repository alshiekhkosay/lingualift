import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

import '../../domain/entities/idiom.dart';
import '../../domain/usecases/create_idiom_use_case.dart';
import '../../domain/usecases/read_one_idiom_use_case.dart';

part 'crud_idiom_event.dart';
part 'crud_idiom_state.dart';

class CrudIdiomBloc extends Bloc<CrudIdiomEvent, CrudIdiomState> {
  final CreateIdiomUseCase createIdiom;
  final ReadOneIdiomUseCase readOneIdiom;
  CrudIdiomBloc(this.createIdiom, this.readOneIdiom) : super(CrudIdiomInitial()) {
    _onEventListener();
  }

  void _onEventListener() {
    on<CrudIdiomEvent>((event, emit) async {
      if (event is CreateIdiomEvent) await _handleCreateIdiomEvent(event, emit);
      if (event is ReadOneIdiomEvent) await _handleReadOneIdiomEvent(event, emit);
    });
  }

  Future<void> _handleCreateIdiomEvent(CreateIdiomEvent event, Emitter<CrudIdiomState> emit) async {
    final createdOrFailure = await createIdiom(IdiomParam(event.idiom));
    createdOrFailure.fold((failure) => print(failure), (r) => print("addded successfully"));
  }

  Future<void> _handleReadOneIdiomEvent(ReadOneIdiomEvent event, Emitter<CrudIdiomState> emit) async {
    final idiomOrFailure = await readOneIdiom(IdiomIdParam(event.id));
    idiomOrFailure.fold((failure) {
      emit(ReadOneIdiomFailed(failure.message));
    }, (idiom) {
      emit(ReadOneIdiomSuccess(idiom));
    });
  }
}
