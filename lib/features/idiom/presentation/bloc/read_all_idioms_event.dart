part of 'read_all_idioms_bloc.dart';

sealed class ReadAllIdiomsEvent extends Equatable {
  const ReadAllIdiomsEvent();

  @override
  List<Object> get props => [];
}

final class ReadAllIdiomEvent extends ReadAllIdiomsEvent {}

