part of 'crud_idiom_bloc.dart';

sealed class CrudIdiomEvent extends Equatable {
  final List<Object> properties;
  const CrudIdiomEvent({this.properties = const []});

  @override
  List<Object> get props => properties;
}

final class CreateIdiomEvent extends CrudIdiomEvent {
  final Idiom idiom;

  CreateIdiomEvent(this.idiom) : super(properties: [idiom]);
}

final class ReadOneIdiomEvent extends CrudIdiomEvent {
  final int id;

  ReadOneIdiomEvent(this.id):super(properties: [id]);
}