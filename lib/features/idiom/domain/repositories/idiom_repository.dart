import 'package:dartz/dartz.dart';

import '../../../../core/error/failure.dart';
import '../entities/idiom.dart';

abstract class IdiomRepository {
  Future<Either<Failure, void>> createIdiom(Idiom idiom);

  Future<Either<Failure, List<Idiom>>> readAllIdioms();

  Future<Either<Failure, Idiom>> readOneIdiom(int id);
}
