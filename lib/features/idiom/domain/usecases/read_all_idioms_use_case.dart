import 'package:dartz/dartz.dart';

import '../../../../core/error/failure.dart';
import '../repositories/idiom_repository.dart';

import '../../../../core/usecase/usecase.dart';
import '../entities/idiom.dart';

class ReadAllIdiomsUseCase extends UseCase<List<Idiom>, NoParams> {
  final IdiomRepository idiomRepository;

  ReadAllIdiomsUseCase(this.idiomRepository);
  @override
  Future<Either<Failure, List<Idiom>>> call(NoParams params) {
    
    return idiomRepository.readAllIdioms();
  }
}
