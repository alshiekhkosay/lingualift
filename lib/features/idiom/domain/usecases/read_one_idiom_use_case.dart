import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import '../../../../core/error/failure.dart';
import '../repositories/idiom_repository.dart';

import '../../../../core/usecase/usecase.dart';
import '../entities/idiom.dart';

class ReadOneIdiomUseCase extends UseCase<Idiom,IdiomIdParam> {
  final IdiomRepository idiomRepository;

  ReadOneIdiomUseCase(this.idiomRepository);
  @override
  Future<Either<Failure, Idiom>> call(IdiomIdParam params) {
    return idiomRepository.readOneIdiom(params.id);
    
  }
  
}

class IdiomIdParam extends Equatable {
  final int id;
  const IdiomIdParam(this.id);

  @override
  List<Object?> get props => [id];
}