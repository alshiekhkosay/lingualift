import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import '../../../../core/error/failure.dart';
import '../entities/idiom.dart';
import '../repositories/idiom_repository.dart';

import '../../../../core/usecase/usecase.dart';

class CreateIdiomUseCase extends UseCase<void, IdiomParam> {
  final IdiomRepository idiomRepository;

  CreateIdiomUseCase(this.idiomRepository);
  @override
  Future<Either<Failure, void>> call(IdiomParam params) {
    return idiomRepository.createIdiom(params.idiom);
  }
}

class IdiomParam extends Equatable {
  final Idiom idiom;

  const IdiomParam(this.idiom);

  @override
  List<Object?> get props => [idiom];
}
