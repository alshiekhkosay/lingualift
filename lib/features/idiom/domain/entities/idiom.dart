import 'package:equatable/equatable.dart';
import '../../../../core/enum/idiom_usage.dart';

class Idiom extends Equatable {
  final int? id;
  final String idiom;
  final String meaning;
  final IdiomUsage usage;
  final List<String> examples;

  const Idiom({
    this.id,
    required this.idiom,
    required this.meaning,
    required this.usage,
    required this.examples,
  });

  static Idiom empty = const Idiom(idiom: "", meaning: "", usage: IdiomUsage.none, examples: []);

  @override
  List<Object?> get props => [id, idiom, meaning, usage, ...examples];
}
