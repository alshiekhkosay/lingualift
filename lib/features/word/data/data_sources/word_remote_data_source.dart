import 'dart:convert';

import '../../../../core/constants/app_urls.dart';
import '../../../../core/error/exeptions.dart';
import '../../../../core/services/api_service.dart';
import '../models/word_model.dart';

abstract class WordRemoteDataSource {
  Future<WordModel> getWordMeaningFromDictionaryApi(String word);
}

class WordRemoteDataSourceImpl implements WordRemoteDataSource {
  final ApiService apiService;

  WordRemoteDataSourceImpl(this.apiService);
  @override
  Future<WordModel> getWordMeaningFromDictionaryApi(String word) async {
    try {
      final response = await apiService.get(AppURLs.wordEntry, id: word);
      if (response.statusCode == 404) {
        final decodedError = jsonDecode(response.body)["message"];
        throw ServerException(message: decodedError);
      }

      final dataMap = jsonDecode(response.body)[0];
      final meaningsList = dataMap["meanings"];

      final meaningsMap = [];

      for (Map meaning in meaningsList) {
        final partOfSpeech = meaning["partOfSpeech"];
        final definations = meaning["definitions"];

        for (Map definiation in definations) {
          meaningsMap.add({
            "partOfSpeech": partOfSpeech,
            "definition": definiation["definition"],
            "example": definiation["example"]
          });
        }
      }

      dataMap["meanings"] = meaningsMap;
      return WordModel.fromMap(dataMap);
    } on Exception {
      rethrow;
    }
  }
}
