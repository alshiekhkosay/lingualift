import 'package:sqflite/sqflite.dart';

import '../../../../core/database/database.dart';
import '../../../../core/error/exeptions.dart';
import '../models/word_model.dart';

abstract class WordLocalDataSource {
  Future<int> createWord(WordModel data);

  Future<bool> checkStored(String word);

  Future<List<WordModel>> readAllWords();

  Future<void> deleteWord(int id);

  Future<WordModel> readOneWord(int? id, String? word);

  Future<void> updateWord(WordModel word);
}

class WordLocalDataSourceImpl implements WordLocalDataSource {
  final DB db;

  WordLocalDataSourceImpl(this.db);
  @override
  Future<int> createWord(WordModel data) async {
    try {
      final wordId = await db.insert(DB.words, data.toMap());
      final meaningsMap = data.meanings.map((meaning) {
        return {
          ...meaning.toMap(),
          'wordId': wordId,
        };
      }).toList();

      final phoneticsMap = data.phonetics.map((phonetic) {
        return {
          ...phonetic.toMap(),
          'wordId': wordId,
        };
      }).toList();

      await db.insertAll(DB.meanings, meaningsMap);
      await db.insertAll(DB.phonetics, phoneticsMap);
      return wordId;
    } on DatabaseException {
      throw CacheException();
    }
  }

  @override
  Future<bool> checkStored(String word) async {
    try {
      final fetchResult = await db.fetch(DB.words, where: 'word LIKE "%$word%"');

      return fetchResult.isNotEmpty;
    } on DatabaseException {
      throw CacheException();
    }
  }

  @override
  Future<List<WordModel>> readAllWords() async {
    try {
      final wordsMaps = [...await db.fetch(DB.words)];
      final List<Map<String, dynamic>> words = [];
      for (var wordMap in wordsMaps) {
        Map<String, dynamic> word = {};
        final meanings = await db.fetch(DB.meanings, where: 'wordId = ${wordMap['id']}');
        final phonetics = await db.fetch(DB.phonetics, where: 'wordId = ${wordMap['id']}');
        word = {
          ...wordMap,
          "meanings": meanings,
          "phonetics": phonetics,
        };
        words.add(word);
      }

      return words.map((word) => WordModel.fromMap(word)).toList();
    } on DatabaseException {
      throw CacheException();
    }
  }

  @override
  Future<void> deleteWord(int id) async {
    try {
      await db.delete(DB.phonetics, where: "wordId = $id");
      await db.delete(DB.meanings, where: "wordId = $id");
      await db.delete(DB.words, where: "id = $id");
    } catch (e) {
      throw CacheException();
    }
  }

  @override
  Future<WordModel> readOneWord(int? id, String? word) async {
    Map<String, dynamic> wordMap = {};
    try {
      if (id != null) {
        wordMap = await _tryReadById(id);
      } else if (word != null) {
        wordMap = await _tryReadByWord(word);
      }

      return WordModel.fromMap(wordMap);
    } on DatabaseException {
      throw CacheException();
    }
  }

  Future<Map<String, dynamic>> _tryReadById(int id) async {
    final wordRows = await db.fetch(DB.words, where: "id = $id");
    if (wordRows.isEmpty) throw CacheException();
    final meaningsRows = await db.fetch(DB.meanings, where: "wordId = $id");
    final phoneticsRows = await db.fetch(DB.phonetics, where: "wordId = $id");
    return {
      ...wordRows[0],
      "meanings": meaningsRows,
      "phonetics": phoneticsRows,
    };
  }

  Future<Map<String, dynamic>> _tryReadByWord(String word) async {
    final wordRows = await db.fetch(DB.words, where: 'word LIKE "%$word%"');
    if (wordRows.isEmpty) throw CacheException();
    final meaningsRows = await db.fetch(DB.meanings, where: 'wordId = ${wordRows[0]['id']}');
    final phoneticsRows = await db.fetch(DB.phonetics, where: 'wordId = ${wordRows[0]['id']}');
    return {
      ...wordRows[0],
      "meanings": meaningsRows,
      "phonetics": phoneticsRows,
    };
  }

  @override
  Future<void> updateWord(WordModel word) async {
    try {
      await db.delete(DB.meanings, where: "wordId = ${word.id}");

      final meaningsMap = word.meanings.map((meaning) {
        return {
          ...meaning.toMap(),
          'wordId': word.id,
        };
      }).toList();
      await db.insertAll(DB.meanings, meaningsMap);
    } on Exception {
      throw CacheException();
    }
  }
}
