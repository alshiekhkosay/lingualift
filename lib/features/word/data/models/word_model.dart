import 'dart:convert';

import 'package:equatable/equatable.dart';

import 'meaning_model.dart';
import 'phonetic_model.dart';

class WordModel extends Equatable {
  final int? id;
  final String word;
  final List<PhoneticModel> phonetics;
  final List<MeaningModel> meanings;
  const WordModel({required this.id,required this.meanings, required this.word, required this.phonetics});

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'word': word,
    };
  }

  factory WordModel.fromMap(Map<String, dynamic> map) {
    return WordModel(
      id: map['id'],
      word: map['word'] as String,
      phonetics: List.from((map['phonetics'] as List).map((phonetic) => PhoneticModel.fromMap(phonetic))),
      meanings: List.from(
        (map['meanings'] as List).map((meaning) => MeaningModel.fromMap(meaning)),
      ),
    );
  }

  String toJson() => json.encode(toMap());

  factory WordModel.fromJson(String source) => WordModel.fromMap(json.decode(source) as Map<String, dynamic>);

  @override
  List<Object?> get props => [word, ...phonetics, ...meanings];
}
