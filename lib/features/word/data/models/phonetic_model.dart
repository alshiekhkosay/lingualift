// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'dart:convert';

import 'package:equatable/equatable.dart';

class PhoneticModel extends Equatable {
  final String audio;
  final String text;

  const PhoneticModel({required this.audio, required this.text});

  @override
  List<Object?> get props => [audio, text];

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'audio': audio,
      'text': text,
    };
  }

  factory PhoneticModel.fromMap(Map<String, dynamic> map) {
  
    return PhoneticModel(
      audio: map['audio'] ?? "",
      text: map['text'] ?? "",
    );
  }

  String toJson() => json.encode(toMap());

  factory PhoneticModel.fromJson(String source) => PhoneticModel.fromMap(json.decode(source) as Map<String, dynamic>);
}
