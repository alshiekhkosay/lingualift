// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'dart:convert';

import 'package:equatable/equatable.dart';
import 'package:lingualift/core/enum/part_of_speech.dart';

class MeaningModel extends Equatable {
  final int? id;
  final PartOfSpeech partOfSpeech;
  final String definition;
  final String? example;
  const MeaningModel({this.id, required this.partOfSpeech, required this.definition, required this.example});

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'partOfSpeech': partOfSpeech.name,
      'definition': definition,
      'example': example,
    };
  }

  factory MeaningModel.fromMap(Map<String, dynamic> map) {
    return MeaningModel(
      id: map['id'],
      partOfSpeech: PartOfSpeech.getPartOfSpeechEnum(map['partOfSpeech'] as String),
      definition: map['definition'] as String,
      example: map['example'],
    );
  }

  String toJson() => json.encode(toMap());

  factory MeaningModel.fromJson(String source) => MeaningModel.fromMap(json.decode(source) as Map<String, dynamic>);

  @override
  List<Object?> get props => [partOfSpeech, definition, example];
}
