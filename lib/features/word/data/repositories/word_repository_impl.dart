import 'package:dartz/dartz.dart';

import '../../../../core/constants/messages.dart';
import '../../../../core/error/exeptions.dart';
import '../../../../core/error/failure.dart';
import '../../../../core/mappers/word_mapper.dart';
import '../../domain/entities/word.dart';
import '../../domain/repositories/word_repository.dart';
import '../data_sources/word_local_data_source.dart';
import '../data_sources/word_remote_data_source.dart';

class WordRepositoryImpl implements WordRepository {
  final WordRemoteDataSource wordRemoteDataSource;
  final WordLocalDataSource wordLocalDataSource;
  WordRepositoryImpl(this.wordRemoteDataSource, this.wordLocalDataSource);

  @override
  Future<Either<Failure, Word>> getWordMeaningFromDictionaryApi(String word) async {
    try {
      final wordModel = await wordRemoteDataSource.getWordMeaningFromDictionaryApi(word);
      final wordDTO = WordDTO.fromData(wordModel);
      return Right(wordDTO.toDomain());
    } on ServerException catch (e) {
      return Left(ServerFailure(e.message));
    } on Exception {
      return const Left(ServerFailure(UNKOWN_ERROR));
    }
  }

  @override
  Future<Either<Failure, int>> createWord(Word word) async {
    try {
      final wordDTO = WordDTO.fromDomain(word);
      return Right(await wordLocalDataSource.createWord(wordDTO.toData()));
    } catch (e) {
      return const Left(CacheFailure(WORD_CREATE_FAILURE_MESSAGE));
    }
  }

  @override
  Future<Either<Failure, bool>> checkStored(String word) async {
    try {
      return Right(await wordLocalDataSource.checkStored(word));
    } on CacheException {
      return const Left(CacheFailure(UNKOWN_ERROR));
    }
  }

  @override
  Future<Either<Failure, List<Word>>> readAllWords() async {
    try {
      final wordsModels = await wordLocalDataSource.readAllWords();
      final wordDTOS = wordsModels.map((wordModel) => WordDTO.fromData(wordModel));
      final words = wordDTOS.map((e) => e.toDomain());

      return Right(words.toList());
    } on CacheException {
      return const Left(CacheFailure(UNKOWN_ERROR));
    }
  }

  @override
  Future<Either<Failure, void>> deleteWord(int id) async {
    try {
      await wordLocalDataSource.deleteWord(id);
      return const Right(null);
    } on Exception {
      return const Left(CacheFailure(UNKOWN_ERROR));
    }
  }

  @override
  Future<Either<Failure, Word>> readOneWord(int? id, String? word) async {
    try {
      final wordModel = await wordLocalDataSource.readOneWord(id, word);
      final wordDTO = WordDTO.fromData(wordModel);
      return Right(wordDTO .toDomain());
    } on CacheException {
      return const Left(CacheFailure(WORD_NOT_FOUND_MESSAGE));
    }
  }

  @override
  Future<Either<Failure, void>> updateWord(Word word) async {
    try {
      final wordDTO = WordDTO.fromDomain(word);
      await wordLocalDataSource.updateWord(wordDTO.toData());
      return const Right(null);
    } on CacheException {
      return const Left(CacheFailure(WORD_NOT_FOUND_MESSAGE));
    }
  }
}
