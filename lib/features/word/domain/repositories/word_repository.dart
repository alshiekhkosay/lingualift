import 'package:dartz/dartz.dart';

import '../../../../core/error/failure.dart';
import '../entities/word.dart';

abstract class WordRepository {
  Future<Either<Failure, Word>> getWordMeaningFromDictionaryApi(String word);

  Future<Either<Failure, int>> createWord(Word word);

  Future<Either<Failure, bool>> checkStored(String word);

  Future<Either<Failure, List<Word>>> readAllWords();

  Future<Either<Failure, void>> deleteWord(int id);

  Future<Either<Failure, Word>> readOneWord(int? id, String? word);

  Future<Either<Failure, void>> updateWord(Word word);
}
