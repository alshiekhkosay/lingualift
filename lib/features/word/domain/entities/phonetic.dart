import 'package:equatable/equatable.dart';

class Phonetic extends Equatable {
  final String audio;
  final String text;

  const Phonetic({required this.audio, required this.text});
  
  @override
  List<Object?> get props => [audio,text];
}