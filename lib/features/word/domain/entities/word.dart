import 'package:equatable/equatable.dart';

import '../../../../core/enum/part_of_speech.dart';
import 'meaning.dart';
import 'phonetic.dart';

class Word extends Equatable {
  final int? id;
  final String word;
  final List<Phonetic> phonetics;
  final List<Meaning> meanings;

  const Word({required this.id, required this.word, required this.phonetics, required this.meanings});

  @override
  List<Object?> get props => [word, ...meanings, ...phonetics];

  Set<PartOfSpeech> get wordPartsOfSpeech {
    return meanings.map((e) => e.partOfSpeech).toSet();
  }

  static Word get empty => const Word(id: null, word: '', phonetics: [], meanings: []);
}
