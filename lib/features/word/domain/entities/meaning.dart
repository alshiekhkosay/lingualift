import 'package:equatable/equatable.dart';

import '../../../../core/enum/part_of_speech.dart';

class Meaning extends Equatable {
  final int? id;
  final PartOfSpeech partOfSpeech;
  final String definition;
  final String? example;

  const Meaning({this.id, required this.partOfSpeech, required this.definition, required this.example});

  @override
  List<Object?> get props => [partOfSpeech, definition, example];
}
