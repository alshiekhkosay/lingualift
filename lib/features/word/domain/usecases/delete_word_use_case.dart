import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';

import '../../../../core/error/failure.dart';
import '../../../../core/usecase/usecase.dart';
import '../repositories/word_repository.dart';

class DeleteWordUseCase extends UseCase<void,WordIdParam>{
  final WordRepository wordRepository;

  DeleteWordUseCase(this.wordRepository);
  @override
  Future<Either<Failure, void>> call(WordIdParam params) {
    return wordRepository.deleteWord(params.id);
  }

}

class WordIdParam extends Equatable {
  final int id;

  const WordIdParam(this.id);
  
  @override
  List<Object?> get props => [id];
}