import 'package:dartz/dartz.dart';

import '../../../../core/error/failure.dart';
import '../../../../core/usecase/usecase.dart';
import '../repositories/word_repository.dart';
import 'word_param.dart';

class CheckStoredUseCase extends UseCase<bool,WordParam>{
  final WordRepository wordRepository;

  CheckStoredUseCase(this.wordRepository);
  @override
  Future<Either<Failure, bool>> call(WordParam params) {
    
    return wordRepository.checkStored(params.word);
  }

}