import 'package:dartz/dartz.dart';

import '../../../../core/error/failure.dart';
import '../../../../core/usecase/usecase.dart';
import '../entities/word.dart';
import '../repositories/word_repository.dart';

class ReadAllWordsUseCase extends UseCase<List<Word>,NoParams>{
  final WordRepository wordRepository;

  ReadAllWordsUseCase(this.wordRepository);
  @override
  Future<Either<Failure, List<Word>>> call(NoParams params) {
    
    return wordRepository.readAllWords();
  }

}