import 'package:dartz/dartz.dart';

import '../../../../core/error/failure.dart';
import '../../../../core/usecase/usecase.dart';
import '../repositories/word_repository.dart';
import 'word_param.dart';

class UpdateWordUseCase extends UseCase<void, WordEntityParam> {
  final WordRepository wordRepository;

  UpdateWordUseCase(this.wordRepository);
  @override
  Future<Either<Failure, void>> call(WordEntityParam params) {
    return wordRepository.updateWord(params.word);
  }
}
