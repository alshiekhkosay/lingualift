import 'package:equatable/equatable.dart';

import '../entities/word.dart';

class WordParam extends Equatable {
  final String word;

  const WordParam(this.word);
  
  @override
  List<Object?> get props => [word];
}

class WordEntityParam extends Equatable {
  final Word word;

  const WordEntityParam(this.word);

  @override
  List<Object?> get props => [word];
}
