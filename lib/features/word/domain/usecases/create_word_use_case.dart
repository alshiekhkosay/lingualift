import 'package:dartz/dartz.dart';

import '../../../../core/error/failure.dart';
import '../../../../core/usecase/usecase.dart';
import '../repositories/word_repository.dart';
import 'word_param.dart';

class CreateWordUseCase extends UseCase<int, WordEntityParam> {
  final WordRepository wordRepository;

  CreateWordUseCase(this.wordRepository);
  @override
  Future<Either<Failure, int>> call(WordEntityParam params) {
    return wordRepository.createWord(params.word);
  }
}
