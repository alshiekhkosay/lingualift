import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';

import '../../../../core/error/failure.dart';
import '../../../../core/usecase/usecase.dart';
import '../entities/word.dart';
import '../repositories/word_repository.dart';

class ReadOneWordUseCase extends UseCase<Word,ConditionParams> {
  final WordRepository wordRepository;

  ReadOneWordUseCase(this.wordRepository);
  @override
  Future<Either<Failure, Word>> call(ConditionParams params) {
    return wordRepository.readOneWord(params.id,params.word);
  }

}

class ConditionParams extends Equatable {
  final int? id;
  final String? word;

  const ConditionParams({this.id, this.word});
  
  @override
  List<Object?> get props => [id,word];
}