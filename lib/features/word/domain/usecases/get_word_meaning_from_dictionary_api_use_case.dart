import 'package:dartz/dartz.dart';

import '../../../../core/error/failure.dart';
import '../../../../core/usecase/usecase.dart';
import '../entities/word.dart';
import '../repositories/word_repository.dart';
import 'word_param.dart';

class GetWordMeaningFromDictionaryApiUseCase extends UseCase<Word,WordParam>{
  final WordRepository wordRepository;

  GetWordMeaningFromDictionaryApiUseCase(this.wordRepository);
  @override
  Future<Either<Failure, Word>> call(WordParam params) {
    return wordRepository.getWordMeaningFromDictionaryApi(params.word);
  }

}

