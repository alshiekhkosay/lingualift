import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';

import '../../../../../core/routes/app_pages.dart';
import '../../../domain/entities/word.dart';
import '../../bloc/crud_word_bloc.dart';

class WordScreenTopBar extends StatelessWidget {
  const WordScreenTopBar({super.key, required this.word});
  final String word;
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 60,
      child: Stack(
        alignment: Alignment.centerLeft,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                child: GestureDetector(
                    onTap: () {
                      Get.back();
                    },
                    child: const Icon(CupertinoIcons.back, size: 32)),
              ),
              BlocBuilder<CrudWordBloc, CrudWordState>(
                builder: (context, state) {
                  return AnimatedOpacity(
                    opacity: state is ReadOneWordSuccess ? 1 : 0,
                    duration: const Duration(milliseconds: 500),
                    curve: Curves.easeInOut,
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20),
                      child: IgnorePointer(
                        ignoring: state is! ReadOneWordSuccess,
                        child: GestureDetector(
                            onTap: () {
                              final word = (state as ReadOneWordSuccess).word;
                              Get.toNamed(Routes.ADD_UPDATE_WORD, arguments: {
                                'word': Word(id: word.id, word: word.word, phonetics: word.phonetics, meanings: List.from(word.meanings)),
                                'isEditing': true
                              });
                            },
                            child: const Icon(Icons.edit_rounded)),
                      ),
                    ),
                  );
                },
              ),
            ],
          ),
          Center(child: Text(word.capitalizeFirst!, style: Theme.of(context).textTheme.headlineLarge)),
        ],
      ),
    );
  }
}
