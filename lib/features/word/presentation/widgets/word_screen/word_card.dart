import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';
import 'package:rive/rive.dart';

import '../../../../../core/enum/part_of_speech.dart';
import '../../../../../core/routes/app_pages.dart';
import '../../../../../core/utils/alerts.dart';
import '../../../domain/entities/meaning.dart';
import '../../../domain/entities/word.dart';
import '../../bloc/crud_word_bloc.dart';
import '../../bloc/read_all_words_bloc.dart';
import 'phonetics.dart';

class WordCard extends StatefulWidget {
  const WordCard({super.key, required this.word});
  final Word word;

  @override
  State<WordCard> createState() => _WordCardState();
}

class _WordCardState extends State<WordCard> {
  SMIInput<bool>? _bookMarkInput;
  Artboard? _riveArtboard;

  @override
  void initState() {
    rootBundle.load('assets/bookmark.riv').then(
      (data) async {
        // Load the RiveFile from the binary data.
        final file = RiveFile.import(data);

        // The artboard is the root of the animation and gets drawn in the
        // Rive widget.
        final artboard = file.mainArtboard;
        var controller = StateMachineController.fromArtboard(artboard, 'State Machine 1');
        if (controller != null) {
          artboard.addController(controller);
          _bookMarkInput = controller.findInput("bookmarked");
        }
        setState(() => _riveArtboard = artboard);
      },
    );

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<CrudWordBloc, CrudWordState>(
      listener: (context, state) {
        if (state is CreateSuccess) {
          context.read<CrudWordBloc>().add(ReadOneWordEvent(id: state.wordId));
          context.read<ReadAllWordsBloc>().add(ReadAllWordEvent());
        }
        if (state is DeleteSuccess) {
          context.read<CrudWordBloc>().add(ReadOneWordEvent(word: widget.word.word));
          context.read<ReadAllWordsBloc>().add(ReadAllWordEvent());
        }
        if (state is CreateFailed) {
          Alerts.showErrorSnackBar(context, state.message);
        }
      },
      child: Stack(
        children: [
          Container(
            margin: const EdgeInsets.all(20),
            padding: const EdgeInsets.symmetric(horizontal: 25, vertical: 10),
            width: double.infinity,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(14),
              boxShadow: [
                BoxShadow(color: Colors.black.withOpacity(0.1), offset: const Offset(2, 5), blurRadius: 12),
              ],
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const SizedBox(height: 20),
                Phonetics(word: widget.word),
                ...meaningsSections(widget.word),
                const SizedBox(height: 10),
              ],
            ),
          ),
          BlocBuilder<CrudWordBloc, CrudWordState>(
            builder: (context, state) {
              if (state is ReadOneWordSuccess) {
                _bookMarkInput?.value = true;
              }
              if (state is ReadOneWordFailed) {
                _bookMarkInput?.value = false;
              }
              return GestureDetector(
                onTap: () async {
                  if (state is ReadOneWordSuccess) {
                    Alerts.showConfirmDialog(context, "are you sure").then((answer) {
                      if (answer != null && answer) {
                        context.read<CrudWordBloc>().add(DeleteWordEvent(state.word.id!));
                      }
                    });
                  }
                  if (state is ReadOneWordFailed) {
                    context.read<CrudWordBloc>().add(CreateWordEvent(widget.word));
                  }
                },
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(top: 13, right: 30),
                      child: SizedBox(
                          height: 48,
                          width: 53,
                          child: _riveArtboard == null
                              ? const SizedBox()
                              : Rive(
                                  artboard: _riveArtboard!,
                                  fit: BoxFit.fill,
                                )),
                    ),
                  ],
                ),
              );
            },
          ),
        ],
      ),
    );
  }

  List<Widget> meaningsSections(Word word) {
    return word.wordPartsOfSpeech.map(
      (partOfSpeech) {
        return Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          Container(
            padding: const EdgeInsets.symmetric(vertical: 4.0, horizontal: 14.0),
            margin: const EdgeInsets.symmetric(vertical: 8.0),
            decoration: BoxDecoration(
              color: Colors.redAccent.shade100,
              borderRadius: BorderRadius.circular(8),
            ),
            child: Text(partOfSpeech.name, style: Theme.of(context).textTheme.titleSmall?.copyWith(color: Colors.white)),
          ),
          ...getMeanings(partOfSpeech, word.meanings)
        ]);
      },
    ).toList();
  }

  List<Widget> getMeanings(PartOfSpeech partOfSpeech, List<Meaning> meanings) {
    final meanignDueToPartOfSpeech = [...meanings.where((element) => element.partOfSpeech == partOfSpeech)];
    return meanignDueToPartOfSpeech.asMap().entries.map((entry) {
      final item = entry.value;
      return Padding(
        padding: const EdgeInsets.symmetric(vertical: 7),
        child: Row(
          children: [
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  RichText(
                    text: TextSpan(
                      text: "${entry.key + 1}. ",
                      style: Theme.of(context).textTheme.labelLarge,
                      children: [
                        TextSpan(text: "${partOfSpeech.name[0]} ", style: Theme.of(context).textTheme.bodyMedium?.copyWith(color: Colors.orange)),
                        TextSpan(text: item.definition, style: Theme.of(context).textTheme.bodySmall),
                      ],
                    ),
                  ),
                  if (item.example != null)
                    Padding(
                      padding: const EdgeInsets.only(top: 3),
                      child: RichText(
                        text: TextSpan(
                          text: "example: ",
                          style: Theme.of(context).textTheme.labelLarge?.copyWith(color: Colors.blue),
                          children: [
                            TextSpan(text: item.example, style: Theme.of(context).textTheme.bodySmall),
                          ],
                        ),
                      ),
                    ),
                ],
              ),
            ),
            const SizedBox(width: 10),
            GestureDetector(
                onTap: () {
                  final url =
                      "https://translate.google.com/?sl=en&tl=ar&text=${item.definition}${item.example != null ? "\nexample: ${item.example}" : ""}&op=translate";
                  Get.toNamed(Routes.WEB_SCREEN, arguments: url);
                },
                child: const Icon(Icons.translate_rounded))
          ],
        ),
      );
    }).toList();
  }
}
