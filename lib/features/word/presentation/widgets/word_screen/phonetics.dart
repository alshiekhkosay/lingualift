import 'package:flutter/material.dart';

import '../../../domain/entities/phonetic.dart';
import '../../../domain/entities/word.dart';

class Phonetics extends StatelessWidget {
  const Phonetics({
    super.key,
    required this.word,
  });

  final Word word;

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Flexible(
          child: Text(
            getPhonetics(word.phonetics),
            style: Theme.of(context).textTheme.headlineMedium,
          ),
        ),
        const SizedBox(width: 10),
        const Icon(Icons.volume_up_outlined, size: 24, color: Colors.blue)
      ],
    );
  }

  String getPhonetics(List<Phonetic> phonetics) {
    return phonetics.map((e) => e.text).join(', ');
  }
}
