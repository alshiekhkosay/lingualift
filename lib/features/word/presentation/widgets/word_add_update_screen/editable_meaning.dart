import 'package:flutter/material.dart';

import '../../../../../core/components/app_text_filed.dart';
import '../../../../../core/components/example_rch_text.dart';
import '../../../domain/entities/meaning.dart';

class EditableMeanging extends StatefulWidget {
  const EditableMeanging({super.key, required this.entry, required this.callBack});
  final MapEntry<int, Meaning> entry;
  final Function(bool, {Meaning? newMeaning}) callBack;
  @override
  State<EditableMeanging> createState() => _EditableMeangingState();
}

class _EditableMeangingState extends State<EditableMeanging> {
  bool editingDefinition = false;
  bool editingExample = false;

  TextEditingController definitionController = TextEditingController();
  TextEditingController exampleController = TextEditingController();

  FocusNode definitionFocusNode = FocusNode();
  FocusNode exampleFocusNode = FocusNode();

  @override
  void initState() {
    definitionController.text = widget.entry.value.definition;
    exampleController.text = widget.entry.value.example ?? "";

    super.initState();
  }

  void addOrUpdate(Meaning item) {
    Meaning meaning = Meaning(
      partOfSpeech: item.partOfSpeech,
      definition: definitionController.text.trim(),
      example: exampleController.text.trim().isEmpty ? null : exampleController.text.trim(),
    );
    widget.callBack(editingDefinition || editingExample, newMeaning: meaning);

    setState(() {
      editingDefinition = false;
      editingExample = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    final item = widget.entry.value;
    return Row(
      children: [
        Expanded(
          child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 7),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                _getDefinitionSection(item),
                _getExampleSection(item),
              ],
            ),
          ),
        ),
        const SizedBox(width: 20),
        GestureDetector(
            onTap: () {
              addOrUpdate(item);
            },
            child: Icon(
              !(editingDefinition || editingExample) ? Icons.delete_outline_rounded : Icons.check_rounded,
            ))
      ],
    );
  }

  Widget _getDefinitionSection(Meaning item) {
    if (!editingDefinition) {
      return _definitionWithoutEditing(item);
    } else {
      return _defnintionEditing();
    }
  }

  Padding _defnintionEditing() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 5),
      child: AppTextField(
        controller: definitionController,
        textStyle: Theme.of(context).textTheme.bodySmall!,
      ),
    );
  }

  GestureDetector _definitionWithoutEditing(Meaning item) {
    return GestureDetector(
      onTap: () {
        setState(() {
          definitionController.text = item.definition;
          definitionFocusNode.requestFocus();
          editingDefinition = true;
        });
      },
      child: DefinitionRichText(entryKey: widget.entry.key, item: item),
    );
  }

  Widget _getExampleSection(Meaning item) {
    if (item.example == null && !editingExample) {
      return addExampleButton();
    } else if (!editingExample) {
      return nonEditingExample(item);
    } else {
      return AppTextField(
        controller: exampleController,
        textStyle: Theme.of(context).textTheme.bodySmall!,
      );
    }
  }

  Padding nonEditingExample(Meaning item) {
    return Padding(
      padding: const EdgeInsets.only(top: 3),
      child: GestureDetector(
        onTap: () {
          setState(() {
            editingExample = true;
          });
          exampleController.text = item.example!;
          exampleFocusNode.requestFocus();
        },
        child: ExampleRichText(
          example: item.example!,
        ),
      ),
    );
  }

  GestureDetector addExampleButton() {
    return GestureDetector(
      onTap: () {
        setState(() {
          editingExample = true;
        });
      },
      child: Text("add example", style: Theme.of(context).textTheme.bodySmall?.copyWith(color: Colors.grey)),
    );
  }
}

class DefinitionRichText extends StatelessWidget {
  const DefinitionRichText({
    super.key,
    required this.entryKey,
    required this.item,
  });

  final int entryKey;
  final Meaning item;

  @override
  Widget build(BuildContext context) {
    return RichText(
      text: TextSpan(
        text: "${entryKey + 1}. ",
        style: const TextStyle(color: Colors.black, fontSize: 16, fontWeight: FontWeight.bold),
        children: [
          TextSpan(text: "${item.partOfSpeech.name[0]} ", style: Theme.of(context).textTheme.bodySmall?.copyWith(color: Colors.orange)),
          TextSpan(text: item.definition, style: Theme.of(context).textTheme.bodySmall),
        ],
      ),
    );
  }
}
