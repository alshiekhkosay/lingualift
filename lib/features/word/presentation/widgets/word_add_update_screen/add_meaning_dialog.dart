import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../../core/components/app_text_filed.dart';
import '../../../../../core/enum/part_of_speech.dart';
import '../../../domain/entities/meaning.dart';

class AddMeaningDialog extends StatefulWidget {
  const AddMeaningDialog({super.key, this.initialPartOfSpeech = PartOfSpeech.noun});
  final PartOfSpeech initialPartOfSpeech;

  @override
  State<AddMeaningDialog> createState() => _AddMeaningDialogState();
}

class _AddMeaningDialogState extends State<AddMeaningDialog> {
  late PartOfSpeech partOfSpeech;
  late GlobalKey<FormState> formKey;

  late TextEditingController definitionController;
  late TextEditingController exampleController;

  @override
  void initState() {
    definitionController = TextEditingController();
    exampleController = TextEditingController();

    partOfSpeech = widget.initialPartOfSpeech;
    formKey = GlobalKey<FormState>();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).unfocus();
      },
      child: Form(
        key: formKey,
        child: Container(
            color: Colors.grey[200],
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 30),
            child: Column(
              children: [
                SizedBox(
                  width: MediaQuery.of(context).size.width,
                  height: 50,
                  child: DropdownButton(
                    items: PartOfSpeech.values
                        .map((e) => DropdownMenuItem(
                              value: e,
                              child: Text(e.name, style: Theme.of(context).textTheme.bodyMedium),
                            ))
                        .toList(),
                    value: null,
                    borderRadius: BorderRadius.circular(15),
                    underline: const SizedBox(),
                    isExpanded: true,
                    hint: Text(partOfSpeech.name, style: Theme.of(context).textTheme.bodyMedium),
                    onChanged: (value) {
                      setState(() {
                        partOfSpeech = value!;
                      });
                    },
                  ),
                ),
                AppTextField(
                  controller: definitionController,
                  textStyle: Theme.of(context).textTheme.bodyMedium!,
                  hintText: "definition",
                  hintStyle: Theme.of(context).textTheme.bodyMedium?.copyWith(color: Colors.grey),
                  validator: (value) {
                    if (value != null && value.trim().isEmpty) {
                      return "this field can not be empty";
                    }
                    return null;
                  },
                ),
                const SizedBox(height: 10),
                AppTextField(
                  controller: exampleController,
                  textStyle: Theme.of(context).textTheme.bodyMedium!,
                  hintText: "example",
                  hintStyle: Theme.of(context).textTheme.bodyMedium?.copyWith(color: Colors.grey),
                ),
                const Spacer(),
                TextButton(
                    onPressed: () {
                      if (formKey.currentState!.validate()) {
                        Meaning meaning = Meaning(
                            partOfSpeech: partOfSpeech,
                            definition: definitionController.text,
                            example: exampleController.text.isEmpty ? null : exampleController.text);
                        Get.back(result: meaning);
                      }
                    },
                    child: Text(
                      "ADD",
                      style: Theme.of(context).textTheme.labelLarge?.copyWith(color: Colors.blue),
                    ))
              ],
            )),
      ),
    );
  }
}
