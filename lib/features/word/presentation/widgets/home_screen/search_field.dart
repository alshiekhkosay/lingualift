import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../bloc/search_words_online_bloc.dart';

class SearchField extends StatelessWidget {
  const SearchField({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(borderRadius: BorderRadius.circular(10), color: Colors.grey[200]),
      alignment: Alignment.center,
      height: 55,
      child: TextField(
        textAlignVertical: TextAlignVertical.center,
        decoration: InputDecoration(
          hintText: "Search online",
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(10), borderSide: BorderSide.none),
          prefixIcon: const Icon(CupertinoIcons.search),
          contentPadding: EdgeInsets.zero,
          suffixIcon: BlocBuilder<SearchWordsOnlineBloc, SearchWordsOnlineState>(
            builder: (context, state) {
              if (state is Loading) {
                return Container(height: 10, width: 10, padding: const EdgeInsets.all(10), child: const CircularProgressIndicator(strokeWidth: 3));
              }
              return const SizedBox();
            },
          ),
        ),
        onSubmitted: (value) {
          context.read<SearchWordsOnlineBloc>().add(GetWordsMeaningFromDictionaryApiEvent(value));
        },
      ),
    );
  }
}
