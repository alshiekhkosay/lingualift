import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../../core/routes/app_pages.dart';
import '../../../domain/entities/word.dart';

class RecentWordCard extends StatelessWidget {
  const RecentWordCard({super.key, required this.word});
  final Word word;
  @override
  Widget build(BuildContext context) {
    return Container(
        height: 175,
        width: 200,
        padding: const EdgeInsets.symmetric(horizontal: 8.0, vertical: 35),
        margin: const EdgeInsets.symmetric(horizontal: 6.0),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(16),
          boxShadow: [
            BoxShadow(color: Colors.black.withOpacity(0.1), offset: const Offset(2, 3), blurRadius: 8),
          ],
        ),
        alignment: Alignment.center,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              word.word,
              style: const TextStyle(fontSize: 25, fontWeight: FontWeight.w400),
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
            ),
            const Spacer(),
            TextButton(
              onPressed: () {
                Get.toNamed(Routes.WORD, arguments: {
                  "word":word
                });
              },
              style: ButtonStyle(
                  backgroundColor: const MaterialStatePropertyAll(Colors.blue),
                  shape: MaterialStatePropertyAll(ContinuousRectangleBorder(borderRadius: BorderRadius.circular(14), side: BorderSide.none)),
                  padding: const MaterialStatePropertyAll(EdgeInsets.symmetric(vertical: 10, horizontal: 30))),
              child: const Text(
                "See Details",
                style: TextStyle(color: Colors.white),
              ),
            )
          ],
        ));
  }
}
