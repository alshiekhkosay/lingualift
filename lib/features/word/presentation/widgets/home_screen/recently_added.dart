
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../bloc/read_all_words_bloc.dart';
import 'recent_word_card.dart';

class RecentlyAdded extends StatelessWidget {
  const RecentlyAdded({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ReadAllWordsBloc, ReadAllWordsState>(
      builder: (context, state) {
        if (state is ReadAllLoading) {
          return const Center(child: CircularProgressIndicator());
        }
        if (state is ReadAllSuccess) {
          return Padding(
            padding: const EdgeInsets.symmetric(horizontal: 24),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const Padding(
                  padding: EdgeInsets.symmetric(vertical: 12),
                  child: Text(
                    "Recently Added",
                    style: TextStyle(fontSize: 28, fontWeight: FontWeight.w500),
                  ),
                ),
                SizedBox(
                  height: 200,
                  child: SingleChildScrollView(
                    scrollDirection: Axis.horizontal,
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: state.words.map((word) => RecentWordCard(word: word)).toList(),
                    ),
                  ),
                ),
              ],
            ),
          );
        }
        return Container();
      },
    );
  }
}