part of 'search_words_online_bloc.dart';

sealed class SearchWordsOnlineEvent extends Equatable {
  final List<Object> properties;
  const SearchWordsOnlineEvent({this.properties = const []});

  @override
  List<Object> get props => properties;
}

final class GetWordsMeaningFromDictionaryApiEvent extends SearchWordsOnlineEvent {
  final String word;

  GetWordsMeaningFromDictionaryApiEvent(this.word) : super(properties: [word]);
}
