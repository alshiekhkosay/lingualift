part of 'crud_word_bloc.dart';

sealed class CrudWordState extends Equatable {
  final List<Object> properties;
  const CrudWordState({this.properties = const []});

  @override
  List<Object> get props => properties;
}

final class CrudWordInitial extends CrudWordState {}

//Create proccess
final class CreateLoading extends CrudWordState {}

final class CreateSuccess extends CrudWordState {
  final int wordId;

  final DateTime dateTime;

  CreateSuccess(this.wordId, this.dateTime) : super(properties: [wordId, dateTime]);
}

final class CreateFailed extends CrudWordState {
  final String message;
  final DateTime dateTime;

  CreateFailed(this.message, this.dateTime) : super(properties: [message, dateTime]);
}

//delete word
final class Deleting extends CrudWordState {}

final class DeleteSuccess extends CrudWordState {}

final class DeleteFailed extends CrudWordState {
  final String message;

  DeleteFailed(this.message) : super(properties: [message]);
}

//read one word
final class ReadOneWordLoading extends CrudWordState {}

final class ReadOneWordSuccess extends CrudWordState {
  final Word word;

  ReadOneWordSuccess(this.word) : super(properties: [word]);
}

final class ReadOneWordFailed extends CrudWordState {
  final String message;

  ReadOneWordFailed(this.message) : super(properties: [message]);
}

//update word
final class UpdatingWord extends CrudWordState {}

final class UpdateSuccess extends CrudWordState {}

final class UpdateFailed extends CrudWordState {}
