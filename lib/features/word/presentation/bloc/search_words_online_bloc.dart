import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

import '../../../../core/error/failure.dart';
import '../../domain/entities/word.dart';
import '../../domain/usecases/check_stored_use_case.dart';
import '../../domain/usecases/get_word_meaning_from_dictionary_api_use_case.dart';
import '../../domain/usecases/word_param.dart';

part 'search_words_online_event.dart';
part 'search_words_online_state.dart';

class SearchWordsOnlineBloc extends Bloc<SearchWordsOnlineEvent, SearchWordsOnlineState> {
  final GetWordMeaningFromDictionaryApiUseCase getWordMeaningFromDictionaryApi;
  final CheckStoredUseCase checkStored;
  SearchWordsOnlineBloc(this.getWordMeaningFromDictionaryApi, this.checkStored) : super(WordsInitial()) {
    on<SearchWordsOnlineEvent>((event, emit) async {
      if (event is GetWordsMeaningFromDictionaryApiEvent) {
        await _handleGetWordMeaningFromDictionaryApiEvent(event, emit);
      }
    });
  }

  Future<void> _handleGetWordMeaningFromDictionaryApiEvent(GetWordsMeaningFromDictionaryApiEvent event, Emitter<SearchWordsOnlineState> emit) async {
    emit(Loading());
    final wordOrFailure = await getWordMeaningFromDictionaryApi(WordParam(event.word));
    final stored = await checkStored(WordParam(event.word));
    wordOrFailure.fold((failure) {
      if (failure is ServerFailure) {
        emit(Failed(failure.message));
      }
    }, (word) {
      stored.fold((_) {
        emit(Success(word, false));
      }, (stored) {
        emit(Success(word, stored));
      });
    });
  }
}
