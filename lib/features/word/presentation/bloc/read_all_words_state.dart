part of 'read_all_words_bloc.dart';

sealed class ReadAllWordsState extends Equatable {
  final List<Object> properties;
  const ReadAllWordsState({this.properties = const []});

  @override
  List<Object> get props => properties;
}

final class ReadAllWordsInitial extends ReadAllWordsState {}

//Read all proccess
final class ReadAllLoading extends ReadAllWordsState {}

final class ReadAllSuccess extends ReadAllWordsState {
  final List<Word> words;

  ReadAllSuccess(this.words) : super(properties: [words]);
}

final class ReadAllFailed extends ReadAllWordsState {
  final String message;
  final DateTime dateTime;

  ReadAllFailed(this.message, this.dateTime) : super(properties: [message, dateTime]);
}
