part of 'search_words_online_bloc.dart';

sealed class SearchWordsOnlineState extends Equatable {
  final List<Object> properties;
  const SearchWordsOnlineState({this.properties = const []});

  @override
  List<Object> get props => properties;
}

final class WordsInitial extends SearchWordsOnlineState {}

final class Loading extends SearchWordsOnlineState {}

final class Success extends SearchWordsOnlineState {
  final Word word;
  final bool stored;
  Success(this.word, this.stored) : super(properties: [word,stored]);
}

final class Failed extends SearchWordsOnlineState {
  final String message;

  Failed(this.message) : super(properties: [message]);
}
