import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

import '../../../../core/error/failure.dart';
import '../../../../core/usecase/usecase.dart';
import '../../domain/entities/word.dart';
import '../../domain/usecases/read_all_words_use_case.dart';

part 'read_all_words_event.dart';
part 'read_all_words_state.dart';

class ReadAllWordsBloc extends Bloc<ReadAllWordsEvent, ReadAllWordsState> {
  
  final ReadAllWordsUseCase readAllWords;
  ReadAllWordsBloc(this.readAllWords) : super(ReadAllWordsInitial()) {
    on<ReadAllWordsEvent>((event, emit) async {
      
      if (event is ReadAllWordEvent) {
        await _handleReadAllEvent(event, emit);
      }
    });
  }

  Future<void> _handleReadAllEvent(ReadAllWordEvent event, Emitter<ReadAllWordsState> emit) async {
    emit(ReadAllLoading());
    final wordsOrFailure = await readAllWords(NoParams());
    wordsOrFailure.fold((failure) {
      if (failure is CacheFailure) {
        emit(ReadAllFailed(failure.message, DateTime.now()));
      }
    }, (words) {
      emit(ReadAllSuccess(words));
    });
  }

}
