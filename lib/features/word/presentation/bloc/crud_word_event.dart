part of 'crud_word_bloc.dart';

sealed class CrudWordEvent extends Equatable {
  final List<Object> properties;
  const CrudWordEvent({this.properties = const []});

  @override
  List<Object> get props => properties;
}

final class CreateWordEvent extends CrudWordEvent {
  final Word word;

  CreateWordEvent(this.word) : super(properties: [word]);
}

final class DeleteWordEvent extends CrudWordEvent {
  final int id;

  DeleteWordEvent(this.id) : super(properties: [id]);
}

final class ReadOneWordEvent extends CrudWordEvent {
  final int? id;
  final String? word;

  ReadOneWordEvent({this.id, this.word}) : super(properties: id == null ? [word!] : [id]);
}

final class UpdateWordEvent extends CrudWordEvent {
  final Word word;

  UpdateWordEvent(this.word) : super(properties: [word]);
}
