import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

import '../../../../core/constants/messages.dart';
import '../../../../core/error/failure.dart';
import '../../domain/entities/word.dart';
import '../../domain/usecases/create_word_use_case.dart';
import '../../domain/usecases/delete_word_use_case.dart';
import '../../domain/usecases/read_one_word_use_case.dart';
import '../../domain/usecases/update_word_use_case.dart';
import '../../domain/usecases/word_param.dart';

part 'crud_word_event.dart';
part 'crud_word_state.dart';

class CrudWordBloc extends Bloc<CrudWordEvent, CrudWordState> {
  final CreateWordUseCase creatWord;
  final DeleteWordUseCase deleteWord;
  final ReadOneWordUseCase readOneWord;
  final UpdateWordUseCase updateWord;
  CrudWordBloc(this.creatWord, this.deleteWord, this.readOneWord, this.updateWord) : super(CrudWordInitial()) {
    on<CrudWordEvent>((event, emit) async {
      if (event is CreateWordEvent) {
        await _handleSaveWordEvent(event, emit);
      }
      if (event is DeleteWordEvent) {
        await _handleDeleteWordEvent(event, emit);
      }
      if (event is ReadOneWordEvent) {
        await _handelReadOneWordEvent(event, emit);
      }
      if (event is UpdateWordEvent) {
        await _handleUpdateWordEvent(event, emit);
      }
    });
  }

  Future<void> _handleSaveWordEvent(CreateWordEvent event, Emitter<CrudWordState> emit) async {
    emit(CreateLoading());
    final createdOrFailure = await creatWord(WordEntityParam(event.word));
    createdOrFailure.fold((failure) {
      emit(CreateFailed(WORD_CREATE_FAILURE_MESSAGE, DateTime.now()));
    }, (wordId) {
      emit(CreateSuccess(wordId, DateTime.now()));
    });
  }

  Future<void> _handleDeleteWordEvent(DeleteWordEvent event, Emitter<CrudWordState> emit) async {
    final deletedOrFailure = await deleteWord(WordIdParam(event.id));
    deletedOrFailure.fold((failure) {
      if (failure is CacheFailure) {
        emit(DeleteFailed(failure.message));
      }
    }, (_) {
      emit(DeleteSuccess());
    });
  }

  Future<void> _handelReadOneWordEvent(ReadOneWordEvent event, Emitter<CrudWordState> emit) async {
    emit(ReadOneWordLoading());
    final wordOrFailure = await readOneWord(ConditionParams(id: event.id, word: event.word));
    wordOrFailure.fold((failure) {
      if (failure is CacheFailure) {
        emit(ReadOneWordFailed(failure.message));
      }
    }, (word) {
      emit(ReadOneWordSuccess(word));
    });
  }

  Future<void> _handleUpdateWordEvent(UpdateWordEvent event, Emitter<CrudWordState> emit) async {
    emit(UpdatingWord());
    final updatedOrFailure = await updateWord(WordEntityParam(event.word));
    updatedOrFailure.fold((l) => emit(UpdateFailed()), (r) => emit(UpdateSuccess()));
  }
}
