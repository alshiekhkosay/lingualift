part of 'read_all_words_bloc.dart';

sealed class ReadAllWordsEvent extends Equatable {
  final List<Object> properties;
  const ReadAllWordsEvent({this.properties = const[]});
  
  @override
  List<Object> get props => properties;
}

final class ReadAllWordEvent extends ReadAllWordsEvent {
  
}