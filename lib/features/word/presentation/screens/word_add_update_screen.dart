import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';
import '../../../../core/enum/part_of_speech.dart';
import '../../../../core/utils/list_extension.dart';
import '../bloc/crud_word_bloc.dart';

import '../../../../core/components/app_button.dart';
import '../../../../core/components/app_text_filed.dart';
import '../../domain/entities/meaning.dart';
import '../../domain/entities/word.dart';
import '../bloc/read_all_words_bloc.dart';
import '../widgets/word_add_update_screen/add_meaning_button.dart';
import '../widgets/word_add_update_screen/add_meaning_dialog.dart';
import '../widgets/word_add_update_screen/editable_meaning.dart';

class WordAddUpdateScreen extends StatefulWidget {
  const WordAddUpdateScreen({super.key});

  @override
  State<WordAddUpdateScreen> createState() => _WordAddUpdateScreenState();
}

class _WordAddUpdateScreenState extends State<WordAddUpdateScreen> {
  late bool isEditing;
  late Word word;
  late List<Meaning> meanings;

  late TextEditingController wordTextController;
  late FocusNode wordFocusNode;

  late GlobalKey<FormState> formKey;
  @override
  void initState() {
    init();
    super.initState();
  }

  void init() {
    wordTextController = TextEditingController();
    wordFocusNode = FocusNode();

    formKey = GlobalKey<FormState>();

    isEditing = Get.arguments['isEditing'];
    if (isEditing) {
      word = Get.arguments['word'];
      wordTextController.text = word.word;
      meanings = word.meanings;
    } else {
      word = Word.empty;
      meanings = [];
    }
  }

  void addOrUpdate() {
    if (!formKey.currentState!.validate()) return;
    if (isEditing) {
      context.read<CrudWordBloc>().add(UpdateWordEvent(Word(id: word.id, word: word.word, meanings: List.from(meanings), phonetics: word.phonetics)));
    } else {
      context
          .read<CrudWordBloc>()
          .add(CreateWordEvent(Word(id: null, word: wordTextController.text.trim(), meanings: List.from(meanings), phonetics: word.phonetics)));
    }
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).unfocus();
      },
      child: BlocListener<CrudWordBloc, CrudWordState>(
        listener: (context, state) {
          if (state is UpdateSuccess) {
            context.read<CrudWordBloc>().add(ReadOneWordEvent(id: word.id, word: word.word));
            Get.back();
          }
          if (state is CreateSuccess) {
            context.read<ReadAllWordsBloc>().add(ReadAllWordEvent());
            Get.back();
          }
        },
        child: PopScope(
          canPop: false,
          onPopInvoked: (didPop) {
            if (!didPop) {
              showDialog(
                context: context,
                builder: (context) {
                  return AlertDialog(
                    contentTextStyle: const TextStyle(fontSize: 16, color: Colors.black),
                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12)),
                    content: const Text("Changes will be discaed"),
                    actions: [
                      GestureDetector(
                        onTap: () {
                          Get.back();
                        },
                        child: const Text("Cancel", style: TextStyle(color: Colors.blue)),
                      ),
                      const SizedBox(width: 10),
                      GestureDetector(
                          onTap: () {
                            Get.back();
                            Get.back();
                          },
                          child: const Text("Ok!", style: TextStyle(color: Colors.blue)))
                    ],
                  );
                },
              );
            }
          },
          child: Scaffold(
            body: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 24),
              child: Form(
                key: formKey,
                child: CustomScrollView(
                  slivers: [
                    SliverToBoxAdapter(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const SizedBox(height: 50),
                          AppTextField(
                            isEditing: !isEditing,
                            controller: wordTextController,
                            textStyle: Theme.of(context).textTheme.bodyLarge!,
                            focusNode: wordFocusNode,
                            hintText: "write you word here",
                            hintStyle: Theme.of(context).textTheme.bodyLarge?.copyWith(color: Colors.grey),
                            validator: (value) {
                              if (value != null && value.trim().isEmpty) {
                                return "this field can not be empty";
                              }
                              return null;
                            },
                          ),
                          const SizedBox(height: 10),
                          ...meaningsSections(),
                          if (meanings.isEmpty)
                            Center(
                              child: Padding(
                                padding: const EdgeInsets.only(top: 10),
                                child: AddButton(
                                  child: Text("Add Meaning", style: Theme.of(context).textTheme.labelMedium),
                                  callBack: () {
                                    showModalBottomSheet<Meaning>(
                                      context: context,
                                      builder: (context) {
                                        return const AddMeaningDialog(initialPartOfSpeech: PartOfSpeech.noun);
                                      },
                                    ).then((meaning) {
                                      wordFocusNode.unfocus();

                                      if (meaning != null) {
                                        meanings.add(meaning);
                                        setState(() {});
                                      }
                                    });
                                  },
                                ),
                              ),
                            ),
                          const SizedBox(height: 10),
                        ],
                      ),
                    ),
                    SliverFillRemaining(
                      hasScrollBody: false,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.end,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          AppButton(
                            callBack: addOrUpdate,
                            child: Text(
                              isEditing ? "Update" : "Add Word",
                              style: Theme.of(context).textTheme.labelLarge?.copyWith(color: Colors.white),
                            ),
                          ),
                          const SizedBox(height: 20)
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  List<Widget> meaningsSections() {
    final existingPartOfSpeech = meanings.map((e) => e.partOfSpeech).toSet();
    final sortedPartOfSpeech = existingPartOfSpeech.toList().sortSublist<PartOfSpeech>(PartOfSpeech.values);
    return sortedPartOfSpeech.map(
      (partOfSpeech) {
        return Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 6.0),
            child: Text(
              partOfSpeech.name.capitalize!,
              style: Theme.of(context).textTheme.titleSmall?.copyWith(color: Colors.orange),
            ),
          ),
          const Divider(
            color: Colors.orange,
            height: 0,
            thickness: 2,
          ),
          ...getMeanings(partOfSpeech, meanings),
          Center(
              child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 10.0),
            child: AddButton(
              child: Text("Add Meaning", style: Theme.of(context).textTheme.labelSmall),
              callBack: () {
                showModalBottomSheet<Meaning>(
                  context: context,
                  builder: (context) {
                    return AddMeaningDialog(initialPartOfSpeech: partOfSpeech);
                  },
                ).then((meaning) {
                  wordFocusNode.unfocus();

                  if (meaning != null) {
                    meanings.add(meaning);
                    setState(() {});
                  }
                });
              },
            ),
          ))
        ]);
      },
    ).toList();
  }

  List<Widget> getMeanings(PartOfSpeech partOfSpeech, List<Meaning> meanings) {
    final meanignDueToPartOfSpeech = [...meanings.where((element) => element.partOfSpeech == partOfSpeech)];
    return meanignDueToPartOfSpeech.asMap().entries.map((entry) {
      return EditableMeanging(
        entry: entry,
        callBack: (edited, {newMeaning}) {
          if (edited) {
            this.meanings[meanings.indexOf(entry.value)] = newMeaning!;
            setState(() {});
            return;
          }
          this.meanings.remove(entry.value);
          setState(() {});
        },
      );
    }).toList();
  }
}
