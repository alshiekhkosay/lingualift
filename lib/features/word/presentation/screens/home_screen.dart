import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';

import '../../../../core/routes/app_pages.dart';
import '../bloc/read_all_words_bloc.dart';
import '../bloc/search_words_online_bloc.dart';
import '../widgets/home_screen/home_screen_top_bar.dart';
import '../widgets/home_screen/recently_added.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  void initState() {
    context.read<ReadAllWordsBloc>().add(ReadAllWordEvent());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<SearchWordsOnlineBloc, SearchWordsOnlineState>(
      listener: (context, state) {
        if (state is Success) {
          Get.toNamed(Routes.WORD, arguments: {"isSearching": true, "word": state.word});
        }
      },
      child: GestureDetector(
        onTap: () {
          FocusScope.of(context).unfocus();
        },
        child: Scaffold(
          backgroundColor: Colors.grey[100],
          floatingActionButton: FloatingActionButton(
            onPressed: () {
              Get.toNamed(Routes.ADD_UPDATE_WORD, arguments: {'isEditing': false});
            },
            backgroundColor: Colors.blue,
            child: const Icon(Icons.add_rounded, color: Colors.white),
          ),
          body: const SingleChildScrollView(
              child: Column(
            children: [
              HomeScreenTopBar(),
              RecentlyAdded(),
            ],
          )),
        ),
      ),
    );
  }
}
