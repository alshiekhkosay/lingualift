import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';

import '../bloc/crud_word_bloc.dart';
import '../bloc/search_words_online_bloc.dart';
import '../widgets/word_screen/word_card.dart';
import '../widgets/word_screen/word_screen_top_bar.dart';

class WordScreen extends StatefulWidget {
  const WordScreen({super.key});

  @override
  State<WordScreen> createState() => _WordScreenState();
}

class _WordScreenState extends State<WordScreen> {
  late bool isSearching;
  @override
  void initState() {
    isSearching = Get.arguments["isSearching"] != null;

    context.read<CrudWordBloc>().add(ReadOneWordEvent(id: Get.arguments["word"]?.id, word: Get.arguments["word"]?.word));

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    if (!isSearching) {
      return Scaffold(
        body: BlocConsumer<CrudWordBloc, CrudWordState>(
          listener: (context, state) {
            if (state is DeleteSuccess) {
              Get.back();
            }
          },
          builder: (context, state) {
            if (state is ReadOneWordLoading) {
              return const Center(child: CircularProgressIndicator());
            }
            if (state is ReadOneWordSuccess) {
              return SafeArea(
                  child: Padding(
                      padding: const EdgeInsets.symmetric(vertical: 10),
                      child: SingleChildScrollView(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            WordScreenTopBar(word: state.word.word),
                            WordCard(word: state.word),
                          ],
                        ),
                      )));
            }
            return Container();
          },
        ),
      );
    } else {
      return Scaffold(
        body: BlocBuilder<SearchWordsOnlineBloc, SearchWordsOnlineState>(
          builder: (context, state) {
            if (state is Success) {
              return SafeArea(
                  child: Padding(
                      padding: const EdgeInsets.symmetric(vertical: 10),
                      child: SingleChildScrollView(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            WordScreenTopBar(word: state.word.word),
                            WordCard(word: state.word),
                          ],
                        ),
                      )));
            }
            return Container();
          },
        ),
      );
    }
  }
}
