// ignore_for_file: unused_element, constant_identifier_names
part of 'app_pages.dart';

abstract class Routes {
  Routes._init();

  static const HOME = _Paths.HOME;
  static const WORD = _Paths.WORD;
  static const ADD_UPDATE_WORD = _Paths.ADD_UPDATE_WORD;
  static const WEB_SCREEN = _Paths.WEB_SCREEN;
  static const ADD_UPDATE_IDIOM = _Paths.ADD_UPDATE_IDIOM;
  static const IDIOM = _Paths.IDIOM;
}

abstract class _Paths {
  _Paths.init();

  static const HOME = "/home";
  static const WORD = "/word";
  static const ADD_UPDATE_WORD = "/add-update-word";
  static const WEB_SCREEN = "/web-screen";
  static const ADD_UPDATE_IDIOM = "/add-update-idiom";
  static const IDIOM = "/idiom";
}
