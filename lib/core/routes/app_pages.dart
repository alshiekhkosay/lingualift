import 'package:flutter/animation.dart';
import 'package:get/get.dart';
import '../../features/idiom/presentation/screens/idiom_screen.dart';

import '../../features/idiom/presentation/screens/idiom_add_update_screen.dart';
import '../../features/web_view/presentation/screens/web_view_screen.dart';
import '../../features/home/presentation/screens/home_screen.dart';
import '../../features/word/presentation/screens/word_add_update_screen.dart';
import '../../features/word/presentation/screens/word_screen.dart';

part 'app_routes.dart';

const Cubic curve = Curves.linearToEaseOut;

class AppPages {
  AppPages._init();

  static final List<GetPage> pages = [
    GetPage(
      name: _Paths.HOME,
      page: () => const HomeScreen(),
      curve: curve,
      transition: Transition.fadeIn,
    ),
    GetPage(
      name: _Paths.WORD,
      page: () => const WordScreen(),
      curve: curve,
      transition: Transition.downToUp,
    ),
    GetPage(
      name: _Paths.ADD_UPDATE_WORD,
      page: () => const WordAddUpdateScreen(),
      curve: curve,
      transition: Transition.downToUp,
    ),
    GetPage(
      name: _Paths.WEB_SCREEN,
      page: () => const WebViewScreen(),
      curve: curve,
      transition: Transition.downToUp,
    ),
    GetPage(
      name: _Paths.ADD_UPDATE_IDIOM,
      page: () => const IdiomAddUpdateScreen(),
      curve: curve,
      transition: Transition.downToUp,
    ),
    GetPage(
      name: _Paths.IDIOM,
      page: () => const IdiomScreen(),
      curve: curve,
      transition: Transition.downToUp,
    ),
  ];
}
