enum IdiomUsage {
  none(""),
  partOfSentence("as part of a sentence"),
  byItSledf('by itself');

  const IdiomUsage(this.text);
  final String text;

  static List<IdiomUsage> getIdioms() {
    return IdiomUsage.values.where((element) => element != IdiomUsage.none).toList();
  }

  static IdiomUsage mapIdiom(String text) {
    return IdiomUsage.values.firstWhere((element) => element.text == text);
  }
}
