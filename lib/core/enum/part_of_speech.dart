enum PartOfSpeech {
  noun,
  verb,
  pronoun,
  adjective,
  adverb,
  preposition,
  conjunction,
  interjection;

  static PartOfSpeech getPartOfSpeechEnum(String name) {
    return PartOfSpeech.values.firstWhere((element) => element.name == name);
  }
}
