import 'package:flutter/material.dart';

abstract class AppFonts {
  //Titles
  static const displayLargeFont = TextStyle(fontSize: 32, fontWeight: FontWeight.bold);
  static const displayMediumFont = TextStyle(fontSize: 30, fontWeight: FontWeight.bold);
  static const displaySmallFont = TextStyle(fontSize: 28, fontWeight: FontWeight.bold);

  //Subtitles
  static const titleLargeFont = TextStyle(fontSize: 24, fontWeight: FontWeight.w600);
  static const titleMediumFont = TextStyle(fontSize: 21, fontWeight: FontWeight.w600);
  static const titleSmallFont = TextStyle(fontSize: 18, fontWeight: FontWeight.w600);

  //Headlines
  static const headLineLargeFont = TextStyle(fontSize: 28, fontWeight: FontWeight.w400);
  static const headLineMediumFont = TextStyle(fontSize: 24, fontWeight: FontWeight.w400);
  static const headLineSmallFont = TextStyle(fontSize: 20, fontWeight: FontWeight.w400);

  //Body
  static const bodyLargeFont = TextStyle(fontSize: 18, fontWeight: FontWeight.w400);
  static const bodyMediumFont = TextStyle(fontSize: 16, fontWeight: FontWeight.w400, height: 1.1);
  static const bodySmallFont = TextStyle(fontSize: 14, fontWeight: FontWeight.w400, height: 1.1);

  //Button
  static const buttonLargeFont = TextStyle(fontSize: 16, fontWeight: FontWeight.w500, height: 1.1);
  static const buttonMediumFont = TextStyle(fontSize: 14, fontWeight: FontWeight.w500);
  static const buttonSmallFont = TextStyle(fontSize: 12, fontWeight: FontWeight.w500);
}
