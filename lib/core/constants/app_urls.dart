abstract class AppURLs {
  static const baseUrl = "https://api.dictionaryapi.dev";
  static const wordEntry = "api/v2/entries/en";
}
