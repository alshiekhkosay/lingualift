// ignore_for_file: constant_identifier_names

const SERVER_EXCEPTION_MESSAGE = "Server error";
const WORD_CREATE_FAILURE_MESSAGE = "The word is already added!";
const WORD_CREATE_SUCCESS_MESSAGE = "saved successfully";
const UNKOWN_ERROR = "Something went wrong";
const WORD_NOT_FOUND_MESSAGE = "word not found in the database";
const IDOM_CREATE_FAILURE_MESSAGE = "idiom alreadey added!";
const IDIOM_NOT_FOUND_MESSAGE = "idiom not found in the database";
