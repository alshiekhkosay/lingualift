import 'dart:convert';
import 'dart:io';

import 'package:http/http.dart';

import '../constants/app_urls.dart';
import '../error/exeptions.dart';

class ApiService {
  static String baseUrl = AppURLs.baseUrl;

  final client = Client();
  ApiService();


  Future<Response> get(String endpoint, {String id = ""}) async {
    final response = await client.get(Uri.parse('$baseUrl/$endpoint/$id'));
    if (response.statusCode == 503) {
      throw ServerException();
    }

    return response;
  }

  Future<Response> create(String endpoint, Map<String, dynamic> data,
      {String id = ""}) async {
    final response = await client.post(Uri.parse('$baseUrl/$endpoint/$id'),
        body: jsonEncode(data),
        headers: {HttpHeaders.contentTypeHeader: 'application/json'});
    if (response.statusCode == 503) {
      throw ServerException();
    }

    return response;
  }

  Future<Response> update(String endpoint, Map<String, dynamic> data,
      {String id = ""}) async {
    final response = await client.put(Uri.parse('$baseUrl/$endpoint/$id'),
        body: jsonEncode(data),
        headers: {HttpHeaders.contentTypeHeader: 'application/json'});
    if (response.statusCode == 503) {
      throw ServerException();
    }

    return response;
  }

  Future<Response> delete(String endpoint, String id) async {
    final response = await client.delete(Uri.parse('$baseUrl/$endpoint/$id'));
    if (response.statusCode == 503) {
      throw ServerException();
    }

    return response;
  }
}
