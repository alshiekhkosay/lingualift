import 'package:flutter/material.dart';
import 'package:get/get.dart';

class Alerts {
  static void showErrorSnackBar(BuildContext context, String message) {
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      content: Text(message),
      backgroundColor: Colors.red,
    ));
  }

  static Future<bool?> showConfirmDialog(BuildContext context, String message) {
    return showDialog<bool>(
      context: context,
      builder: (context) => AlertDialog(
          contentTextStyle: const TextStyle(fontSize: 16, color: Colors.black),
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12)),
          content: Text(message),
          actions: [
            GestureDetector(
              onTap: () {
                Get.back(result: false);
              },
              child: const Text("Cancel", style: TextStyle(color: Colors.blue)),
            ),
            const SizedBox(width: 10),
            GestureDetector(
                onTap: () {
                  Get.back(result: true);
                },
                child: const Text("Ok!", style: TextStyle(color: Colors.blue)))
          ]),
    );
  }
}
