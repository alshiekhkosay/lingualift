extension SubList on List {
  List<T> sortSublist<T>(List<T> parentList) {
    final Map<T, int> indexMap = {for (var i = 0; i < parentList.length; i++) parentList[i]: i};

    sort((a, b) => indexMap[a]!.compareTo(indexMap[b]!));

    return this as List<T>;
  }
}
