import 'package:flutter/material.dart';

class AppTextField extends StatelessWidget {
  const AppTextField(
      {super.key,
      this.isEditing = true,
      required this.controller,
      required this.textStyle,
      this.focusNode,
      this.hintText,
      this.hintStyle,
      this.validator});
  final bool isEditing;
  final TextEditingController controller;
  final TextStyle textStyle;
  final FocusNode? focusNode;
  final String? hintText;
  final TextStyle? hintStyle;
  final String? Function(String?)? validator;
  @override
  Widget build(BuildContext context) {
    return TextFormField(
        validator: validator,
        textAlignVertical: TextAlignVertical.center,
        enabled: isEditing,
        maxLines: null,
        controller: controller,
        focusNode: focusNode,
        style: textStyle,
        decoration: InputDecoration(
          border: const UnderlineInputBorder(borderSide: BorderSide(color: Colors.blue)),
          focusedBorder: const UnderlineInputBorder(borderSide: BorderSide(color: Colors.blue)),
          disabledBorder: InputBorder.none,
          hintText: hintText,
          hintStyle: hintStyle,
        ));
  }
}
