import 'package:flutter/material.dart';

class ExampleRichText extends StatelessWidget {
  const ExampleRichText({
    super.key,
    required this.example,
  });

  final String example;

  @override
  Widget build(BuildContext context) {
    return RichText(
      text: TextSpan(
        text: "example: ",
        style: Theme.of(context).textTheme.labelMedium?.copyWith(color: Colors.blue),
        children: [TextSpan(text: example, style: Theme.of(context).textTheme.bodySmall)],
      ),
    );
  }
}
