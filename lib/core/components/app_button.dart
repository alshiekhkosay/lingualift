import 'package:flutter/material.dart';

class AppButton extends StatelessWidget {
  const AppButton({
    super.key,
    required this.callBack,
    required this.child,
  });

  final Function() callBack;
  final Widget child;
  @override
  Widget build(BuildContext context) {
    return TextButton(
        onPressed: callBack,
        style: ButtonStyle(
            shape: MaterialStatePropertyAll(RoundedRectangleBorder(borderRadius: BorderRadius.circular(8))),
            backgroundColor: const MaterialStatePropertyAll(Colors.orange),
            padding: const MaterialStatePropertyAll(EdgeInsets.symmetric(horizontal: 20))),
        child: child);
  }
}
