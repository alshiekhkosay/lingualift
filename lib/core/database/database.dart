import 'dart:async';

import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

class DB {
  static final DB instance = DB._init();

  DB._init();

  static const words = "words";
  static const meanings = "meanings";
  static const phonetics = "phonetics";
  static const idioms = "idioms";
  static Database? _database;

  Future<Database> get database async {
    if (_database != null) return _database!;

    _database = await _initDB('words.db');
    return _database!;
  }

  Future<Database> _initDB(String filepath) async {
    final dbPath = await getDatabasesPath();
    final path = join(dbPath, filepath);

    return await openDatabase(path, version: 1, onCreate: _createDB);
  }

  Future<FutureOr<void>> _createDB(Database db, int version) async {
    await db.execute('''
CREATE TABLE $words (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  word TEXT NOT NULL UNIQUE
)
''');

    await db.execute('''
CREATE TABLE $meanings (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  partOfSpeech TEXT NOT NULL,
  definition TEXT NOT NULL,
  example TEXT,
  wordId INTEGER NOT NULL
)
''');

    await db.execute('''
CREATE TABLE $phonetics (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  audio TEXT,
  text TEXT,
  wordId INTEGER NOT NULL
)
''');

    await db.execute('''
CREATE TABLE $idioms (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  idiom Text NOT NULL UNIQUE,
  meaning TEXT NOT NULL,
  examples TEXT,
  usage TEXT NOT NULL
)
''');
  }

  Future<int> insert(String table, Map<String, dynamic> row) async {
    final db = await instance.database;

    return await db.insert(table, row);
  }

  Future insertAll(String table, List<Map<String, dynamic>> rows) async {
    for (var row in rows) {
      try {
        await insert(table, row);
      } on Exception {
        continue;
      }
    }
  }

  Future<List<Map<String, Object?>>> fetch(String table, {String? where}) async {
    final db = await instance.database;
    return await db.query(table, where: where);
  }

  Future delete(String table, {String? where}) async {
    final db = await instance.database;
    db.delete(table, where: where);
  }

  Future executeRawQuery(String rawQuery) async {
    final db = await instance.database;
    return db.rawQuery(rawQuery);
  }
}
