import '../../features/idiom/data/models/idiom_model.dart';

import '../../features/idiom/domain/entities/idiom.dart';
import '../enum/idiom_usage.dart';

class IdiomDTO {
  final int? id;
  final String idiom;
  final String meaning;
  final IdiomUsage usage;
  final List<String> examples;

  IdiomDTO({
    required this.id,
    required this.idiom,
    required this.meaning,
    required this.usage,
    required this.examples,
  });

  factory IdiomDTO.fromDomain(Idiom idiom) {
    return IdiomDTO(
      id: idiom.id,
      idiom: idiom.idiom,
      meaning: idiom.meaning,
      usage: idiom.usage,
      examples: idiom.examples,
    );
  }

  IdiomModel toData() {
    return IdiomModel(id: id, idiom: idiom, meaning: meaning, usage: usage, examples: examples);
  }
}
