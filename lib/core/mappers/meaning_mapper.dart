import '../../features/word/data/models/meaning_model.dart';
import '../../features/word/domain/entities/meaning.dart';
import '../enum/part_of_speech.dart';

class MeaningDTO {
  final int? id;
  final PartOfSpeech partOfSpeech;
  final String definition;
  final String? example;

  const MeaningDTO({this.id, required this.partOfSpeech, required this.definition, required this.example});

  factory MeaningDTO.fromDomain(Meaning meaning) {
    return MeaningDTO(
      id: meaning.id,
      partOfSpeech: meaning.partOfSpeech,
      definition: meaning.definition,
      example: meaning.example,
    );
  }
  factory MeaningDTO.fromData(MeaningModel meaningModel) {
    return MeaningDTO(
      id: meaningModel.id,
      partOfSpeech: meaningModel.partOfSpeech,
      definition: meaningModel.definition,
      example: meaningModel.example,
    );
  }

  MeaningModel toData() {
    return MeaningModel(id: id, partOfSpeech: partOfSpeech, definition: definition, example: example);
  }
  Meaning toDomain() {
    return Meaning(id: id, partOfSpeech: partOfSpeech, definition: definition, example: example);
  }

  
}
