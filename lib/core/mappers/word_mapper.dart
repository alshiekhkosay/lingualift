import '../../features/word/data/models/word_model.dart';
import '../../features/word/domain/entities/word.dart';
import 'meaning_mapper.dart';
import 'phonetic_mapper.dart';

class WordDTO {
  final int? id;
  final String word;
  final List<PhoneticDTO> phonetics;
  final List<MeaningDTO> meanings;

  const WordDTO({this.id, required this.word, required this.phonetics, required this.meanings});

  factory WordDTO.fromDomain(Word word) {
    return WordDTO(
      id: word.id,
      word: word.word,
      phonetics: word.phonetics.map((e) => PhoneticDTO.fromDomain(e)).toList(),
      meanings: word.meanings.map((e) => MeaningDTO.fromDomain(e)).toList(),
    );
  }

  factory WordDTO.fromData(WordModel wordModel) {
    return WordDTO(
      id: wordModel.id,
      word: wordModel.word,
      phonetics: wordModel.phonetics.map((e) => PhoneticDTO.fromData(e)).toList(),
      meanings: wordModel.meanings.map((e) => MeaningDTO.fromData(e)).toList(),
    );
  }
  
  WordModel toData() {
    return WordModel(
      id: id,
      word: word,
      phonetics: phonetics.map((e) => e.toData()).toList(),
      meanings: meanings.map((e) => e.toData()).toList(),
    );
  }

  Word toDomain() {
    return Word(
      id: id,
      word: word,
      phonetics: phonetics.map((e) => e.toDomain()).toList(),
      meanings: meanings.map((e) => e.toDomain()).toList(),
    );
  }
}
