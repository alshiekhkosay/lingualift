import '../../features/word/data/models/phonetic_model.dart';
import '../../features/word/domain/entities/phonetic.dart';

class PhoneticDTO {
  final String audio;
  final String text;

  PhoneticDTO({required this.audio, required this.text});

  factory PhoneticDTO.fromDomain(Phonetic phonetic) {
    return PhoneticDTO(
      audio: phonetic.audio,
      text: phonetic.text,
    );
  }

  factory PhoneticDTO.fromData(PhoneticModel phoneticModel) {
    return PhoneticDTO(
      audio: phoneticModel.audio,
      text: phoneticModel.text,
    );
  }
  

  PhoneticModel toData() {
    return PhoneticModel(
      audio: audio,
      text: text,
    );
  }

   Phonetic toDomain() {
    return Phonetic(
      audio: audio,
      text: text,
    );
  }
}
