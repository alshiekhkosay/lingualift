import '../constants/messages.dart';

class ServerException implements Exception {
  final String message;

  ServerException({this.message = SERVER_EXCEPTION_MESSAGE});
}

class CacheException implements Exception {
  CacheException();
}
