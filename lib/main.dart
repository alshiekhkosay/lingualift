import 'features/idiom/presentation/bloc/crud_idiom_bloc.dart';
import 'features/idiom/presentation/bloc/read_all_idioms_bloc.dart';

import 'core/constants/app_fonts.dart';
import 'core/routes/app_pages.dart';

import 'features/word/presentation/bloc/read_all_words_bloc.dart';

import 'features/word/presentation/bloc/crud_word_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';

import 'features/word/presentation/bloc/search_words_online_bloc.dart';
import 'injection_container.dart';
import 'injection_container.dart' as di;

void main() {
  di.init();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<SearchWordsOnlineBloc>(create: (context) => sl()),
        BlocProvider<CrudWordBloc>(create: (context) => sl()),
        BlocProvider<ReadAllWordsBloc>(create: (context) => sl()),
        BlocProvider<ReadAllIdiomsBloc>(create: (context) => sl()),
        BlocProvider<CrudIdiomBloc>(create: (context) => sl()),
      ],
      child: GetMaterialApp(
        title: 'A-Z Words',
        theme: ThemeData(
            colorScheme: ColorScheme.fromSeed(seedColor: Colors.blue),
            scaffoldBackgroundColor: Colors.grey[100],
            useMaterial3: true,
            textTheme: const TextTheme(
              displayLarge: AppFonts.displayLargeFont,
              displayMedium: AppFonts.displayMediumFont,
              displaySmall: AppFonts.displaySmallFont,
              titleLarge: AppFonts.titleLargeFont,
              titleMedium: AppFonts.titleMediumFont,
              titleSmall: AppFonts.titleSmallFont,
              bodyLarge: AppFonts.bodyLargeFont,
              bodyMedium: AppFonts.bodyMediumFont,
              bodySmall: AppFonts.bodySmallFont,
              labelLarge: AppFonts.buttonLargeFont,
              labelMedium: AppFonts.buttonMediumFont,
              labelSmall: AppFonts.buttonSmallFont,
              headlineLarge: AppFonts.headLineLargeFont,
              headlineMedium: AppFonts.headLineMediumFont,
              headlineSmall: AppFonts.headLineSmallFont,
            )),
        getPages: AppPages.pages,
        initialRoute: Routes.HOME,
      ),
    );
  }
}
